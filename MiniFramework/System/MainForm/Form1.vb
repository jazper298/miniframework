﻿Imports System.Reflection
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid

Public Class Form1


    Private Sub TreeView1_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try
            Dim asm = Assembly.GetExecutingAssembly
            Dim presenter As Type = Nothing
            Dim list As New Object
            Dim frm As New Form
            Dim gControl As GridControl = Nothing
            Dim gView As New Object
            Dim dr As Object = Nothing

            If e.Node.Tag <> "" Then


                For Each item As Control In SplitContainer1.Panel2.Controls
                    If TypeOf item Is Form Then
                        frm = item
                        Exit For
                    End If
                Next


                For Each ctl As Control In frm.Controls
                    If TypeOf ctl Is GridControl Then
                        gControl = ctl
                    Else
                        gView = ctl
                    End If
                Next

                If gControl IsNot Nothing Then
                    gView = New GridView(gControl)
                    gControl.ViewCollection.Add(gView)
                    gView = gControl.MainView
                End If


                If e.Node.Text.Contains("List") Then
                    FillPanel(getForm(e.Node.Tag))
                    Exit Sub
                End If


                If e.Node.Text.Contains("Picture") Then
                    FillPanel(getForm(e.Node.Tag))
                    Exit Sub
                End If

                If e.Node.Text.Contains("Report") Then
                    FillPanel(getForm(e.Node.Tag))
                    Exit Sub
                End If

                If e.Node.Text.Contains("Add") Then

                    For Each p As PropertyInfo In frm.GetType().GetProperties
                        If p.ToString.ToLower.Contains("present") Then
                            presenter = asm.GetType(p.PropertyType.FullName.ToString)
                        End If
                    Next

                    list = Activator.CreateInstance(presenter, frm, True)
                    executeFunction(e.Node.Tag, list)
                Else

                    If TypeOf gView Is GridView Then
                        dr = gView.GetFocusedRow
                    End If

                    For Each p As PropertyInfo In frm.GetType().GetProperties
                        If p.ToString.ToLower.Contains("present") Then
                            presenter = asm.GetType(p.PropertyType.FullName.ToString)
                        End If
                    Next

                    list = Activator.CreateInstance(presenter, frm, True)
                    executeFunction(e.Node.Tag, list, dr)

                End If

                ''
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Public Function executeFunction(URL As String, ByVal params As Object) As Object

        Dim result As New Object
        Dim path As String = Application.StartupPath & "\"
        Dim assemblyURL As String
        Dim methodURL As String
        Dim tempURL() As String = URL.Split(".")

        Try
            assemblyURL = tempURL(0)
            methodURL = tempURL(tempURL.Count - 1)
            Dim asm = Assembly.GetExecutingAssembly

            For i As Integer = 1 To tempURL.Count - 2
                assemblyURL &= "." & tempURL(i)
            Next

            Dim type = asm.GetType(assemblyURL)

            Dim obj = Activator.CreateInstance(type)
            result = CallByName(obj, methodURL, CallType.Method, params)

        Catch ex As Exception
            MsgBox("test" + ex.Message)
        End Try

        Return result
    End Function

    Public Sub executeFunction(URL As String, ByVal params1 As Object, ByVal params2 As Object)

        Dim result As New Object
        Dim path As String = Application.StartupPath & "\"
        Dim assemblyURL As String
        Dim methodURL As String

        Dim tempURL() As String = URL.Split(".")
        Try
            assemblyURL = tempURL(0)
            methodURL = tempURL(tempURL.Count - 1)
            Dim asm = Assembly.GetExecutingAssembly

            For i As Integer = 1 To tempURL.Count - 2
                assemblyURL &= "." & tempURL(i)
            Next

            Dim type = asm.GetType(assemblyURL)

            Dim obj = Activator.CreateInstance(type)
            result = CallByName(obj, methodURL, CallType.Method, params1, params2)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Function getForm(URL As String) As Form
        Dim frm As New Form
        Dim path As String = Application.StartupPath & "\"
        Dim assemblyURL As String
        Dim methodURL As String

        Dim tempURL() As String = URL.Split(".")
        Try
            assemblyURL = tempURL(0)
            methodURL = tempURL(tempURL.Count - 1)
            Dim asm = Assembly.GetExecutingAssembly

            For i As Integer = 1 To tempURL.Count - 2
                assemblyURL &= "." & tempURL(i)
            Next
            Dim type = asm.GetType(assemblyURL)
            Dim obj = Activator.CreateInstance(type)
            frm = CallByName(obj, methodURL, CallType.Method)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return frm
    End Function


    Public Sub FillPanel(ByVal FORM As Form)
        Try
            FORM.ControlBox = False
            FORM.TopLevel = False

            FORM.Dock = DockStyle.Fill
            FORM.FormBorderStyle = FormBorderStyle.None
            'viewMain.ListPanel.Controls.Clear()
            SplitContainer1.Panel2.Controls.Add(FORM)
            'FORM.TabIndex = 5

            FORM.TabStop = True
            For Each ctl As Control In FORM.Controls
                If TypeOf ctl Is GridControl Then
                    'grid = DirectCast(ctl, GridControl)
                    'RemoveHandler grid.DataSourceChanged, AddressOf rowCount
                    'AddHandler grid.DataSourceChanged, AddressOf rowCount

                    'Dim gView As GridView
                    'gView = New GridView(grid)
                    'grid.ViewCollection.Add(gView)
                    'gView = grid.MainView

                    'AddHandler gView.SelectionChanged, AddressOf selectChange
                    'AddHandler gView.RowClick, AddressOf rowClick
                    ''AddHandler gView.FocusedRowChanged, AddressOf rowChange                   


                    'ctl.ContextMenuStrip = ContextMenuStrip
                End If
            Next

            FORM.Show()
            FORM.BringToFront()
            FORM.KeyPreview = True



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
    End Sub

    Private Sub SplitContainer1_Panel2_Paint(sender As Object, e As PaintEventArgs) Handles SplitContainer1.Panel2.Paint

    End Sub
End Class
