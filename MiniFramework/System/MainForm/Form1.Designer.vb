﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("List")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("View")
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Add")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Edit")
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete")
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Deactivate")
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Reactivate")
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Bank Setup", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3, TreeNode4, TreeNode5, TreeNode6, TreeNode7})
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("List")
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node0", New System.Windows.Forms.TreeNode() {TreeNode9})
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("List Form")
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Add Person")
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Edit Person")
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete Person")
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Dectivate")
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Reactivate")
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Picture")
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Add Journal")
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Journal List")
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Edit Entries")
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete Entries")
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Person Setup", New System.Windows.Forms.TreeNode() {TreeNode11, TreeNode12, TreeNode13, TreeNode14, TreeNode15, TreeNode16, TreeNode17, TreeNode18, TreeNode19, TreeNode20, TreeNode21})
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(2)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TreeView1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Size = New System.Drawing.Size(876, 492)
        Me.SplitContainer1.SplitterDistance = 181
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 0
        '
        'TreeView1
        '
        Me.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView1.Location = New System.Drawing.Point(0, 0)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeView1.Name = "TreeView1"
        TreeNode1.Name = "Node7"
        TreeNode1.Tag = "Mini.BankSetupFactory.ListForm"
        TreeNode1.Text = "List"
        TreeNode2.Name = "Node0"
        TreeNode2.Tag = "Mini.BankSetupFactory.ViewForm"
        TreeNode2.Text = "View"
        TreeNode3.Name = "Node2"
        TreeNode3.Tag = "Mini.BankSetupFactory.AddForm"
        TreeNode3.Text = "Add"
        TreeNode4.Name = "Node3"
        TreeNode4.Tag = "Mini.BankSetupFactory.EditForm"
        TreeNode4.Text = "Edit"
        TreeNode5.Name = "Node4"
        TreeNode5.Tag = "Mini.BankSetupFactory.Delete"
        TreeNode5.Text = "Delete"
        TreeNode6.Name = "Node5"
        TreeNode6.Tag = "Mini.BankSetupFactory.Deactivate"
        TreeNode6.Text = "Deactivate"
        TreeNode7.Name = "Node6"
        TreeNode7.Tag = "Mini.BankSetupFactory.Reactivate"
        TreeNode7.Text = "Reactivate"
        TreeNode8.Name = "Node0"
        TreeNode8.Text = "Bank Setup"
        TreeNode9.Name = "Node1"
        TreeNode9.Tag = "Mini.RPTPenaltyTableFactory.ListForm"
        TreeNode9.Text = "List"
        TreeNode10.Name = "Node0"
        TreeNode10.Text = "Node0"
        TreeNode11.Name = "Node1"
        TreeNode11.Tag = "Mini.PersonSetupFactory.ListForm"
        TreeNode11.Text = "List Form"
        TreeNode12.Name = "Node2"
        TreeNode12.Tag = "Mini.PersonSetupFactory.AddForm"
        TreeNode12.Text = "Add Person"
        TreeNode13.Name = "Node3"
        TreeNode13.Tag = "Mini.PersonSetupFactory.EditForm"
        TreeNode13.Text = "Edit Person"
        TreeNode14.Name = "Node0"
        TreeNode14.Tag = "Mini.PersonSetupFactory.Delete"
        TreeNode14.Text = "Delete Person"
        TreeNode15.Name = "Node3"
        TreeNode15.Tag = "Mini.PersonSetupFactory.Deactivate"
        TreeNode15.Text = "Dectivate"
        TreeNode16.Name = "Node5"
        TreeNode16.Tag = "Mini.PersonSetupFactory.Reactivate"
        TreeNode16.Text = "Reactivate"
        TreeNode17.Name = "Node1"
        TreeNode17.Tag = "Mini.PersonSetupFactory.Picture"
        TreeNode17.Text = "Picture"
        TreeNode18.Name = "Node3"
        TreeNode18.Tag = "Mini.JournalEntryFactory.Report"
        TreeNode18.Text = "Add Journal"
        TreeNode19.Name = "Node1"
        TreeNode19.Tag = "Mini.JournalEntryFactory.JournalList"
        TreeNode19.Text = "Journal List"
        TreeNode20.Name = "Node1"
        TreeNode20.Tag = "Mini.JournalEntryFactory.EditEntries"
        TreeNode20.Text = "Edit Entries"
        TreeNode21.Name = "Node3"
        TreeNode21.Tag = "Mini.JournalEntryFactory.DeleteEntries"
        TreeNode21.Text = "Delete Entries"
        TreeNode22.Name = "Journal List"
        TreeNode22.Text = "Person Setup"
        Me.TreeView1.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode8, TreeNode10, TreeNode22})
        Me.TreeView1.Size = New System.Drawing.Size(181, 492)
        Me.TreeView1.TabIndex = 0
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(876, 492)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mini"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents TreeView1 As TreeView
End Class
