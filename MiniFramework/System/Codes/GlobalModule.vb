﻿Imports Npgsql
Imports System.Configuration
Imports DevExpress.Data.XtraReports.Wizard
Imports DevExpress.XtraGrid

Module GlobalModule
    Public title = "Nano ERP v.2.0"
    Public SessionUserId As Integer
    Public SessionID As Integer
    Public _SessionRoleTypeID As Integer
    Public _global_UserName As String = ""
    Public _global_RoleName As String = ""
    Public _global_ConnectionStatus As String = ""
    Public _global_row_count As Integer
    Public sessionRecordActivityID

    Public dtUser As DataTable
    Public dt_ActivityUrl As DataTable
    Public dt_ObjectList As DataTable

    Public Dt_GetDomain As DataTable
    Public Dt_GetDomain_Software_Setting As DataTable
    Public DT_getDomain_Software_Setting_ As DataTable
    Public drrrr As DataTable
    Public dr12rrr As DataTable

    Public SessionUserVersion As Integer = 0

    Public GlobalPersonalSetting As DataTable

    Public GlobalProducType As DataTable

    ' Public global_objectList As ObjectList

    Public AffectedObject As New List(Of Object)

    Public _expandNode As New List(Of String)
    Public _expandNodeID As New List(Of Integer)

    Public _hide As New List(Of String)
    Public _Unhide As New List(Of String)

    Public copyright As String = "Copyright © 2017 ELF STATION, INC."
    Dim ger2x As String = ""
    Public preparedby As String

    Public Property projecTitle
        Get
            Return title
        End Get
        Set(value)
            title = value
        End Set
    End Property

    'Public Property global_Title
    '    Get
    '        Return MainForm.Text
    '    End Get
    '    Set(value)
    '        MainForm.Text = title & " - " & value
    '    End Set
    'End Property

    'Public Property global_row_count
    '    Get
    '        Return _global_row_count
    '    End Get
    '    Set(value)
    '        _global_row_count = value
    '        If value > 0 Then
    '            MainForm.ToolStripRow.Text = value & " row(s)"
    '        Else
    '            MainForm.ToolStripRow.Text = ""
    '        End If
    '    End Set
    'End Property

    'Public Property global_ConnectionStatus
    '    Get
    '        Return _global_ConnectionStatus
    '    End Get
    '    Set(value)
    '        _global_ConnectionStatus = value
    '        MainForm.ToolStripStatus.Text = "Status: " & value
    '    End Set
    'End Property

    'Public Property global_UserName
    '    Get
    '        Return _global_UserName
    '    End Get
    '    Set(value)
    '        _global_UserName = value
    '        MainForm.ToolStripUser.Text = "User: " & value
    '    End Set
    'End Property

    'Public Property global_RoleName
    '    Get
    '        Return _global_RoleName
    '    End Get
    '    Set(value)
    '        _global_RoleName = value
    '        MainForm.ToolStripRole.Text = "Role: " & value
    '    End Set
    'End Property

    'Public Property SessionRoleTypeID
    '    Get
    '        Return _SessionRoleTypeID
    '    End Get
    '    Set(value)
    '        If _SessionRoleTypeID <> value Then

    '            If value = 0 Then
    '                SessionRoleType.EndSessionRoleInvocation()
    '            End If

    '            If SessionUserId > 0 Then
    '                _SessionRoleTypeID = value
    '                Login_Model.getUserObjectList()
    '                SessionRoleType.StartSessionRoleInvocation()
    '            End If

    '        End If

    '    End Set
    'End Property

    'Public Function UserAuthorization()
    '    Dim sql As String = ""
    '    Dim dt As DataTable
    '    Dim bool As Boolean = True

    '    sql = "SELECT * FROM ""User"" WHERE ""UserID""  = '" & SessionUserId & "' "
    '    dt = DataSource(sql)

    '    If dt(0)("Version") <> SessionUserVersion Then
    '        If MsgBox("Your Account has been changed, The administrator change some of your privilege. Press OK to continue", MsgBoxStyle.Information, "Account Authorization") = MsgBoxResult.Ok Then
    '            MainForm.ToolStrip1.Items("ERP.Refresh.doRefresh").PerformClick()
    '            SessionUserVersion = dt(0)("Version")
    '            bool = False
    '        End If
    '    End If

    '    Return bool
    'End Function


    'Dim _SessionActivityTypeID As ActivityType
    'Public Property SessionActivityTypeID
    '    Get
    '        Return _SessionActivityTypeID
    '    End Get
    '    Set(value)
    '        If _SessionActivityTypeID Is Nothing Then
    '            _SessionActivityTypeID = value

    '            If _SessionActivityTypeID.ActivityTypeID > 0 Then
    '                SessionModel.ActivitySession(_SessionActivityTypeID)
    '            End If
    '        Else
    '            SessionModel.ActivitySession(value)
    '            _SessionActivityTypeID = value
    '        End If


    '    End Set
    'End Property


    Public Sub FormProperty(form As Form, ByVal Optional bool As Boolean = False)
        Dim isDraft As Boolean = True
        For Each ctl As Control In form.Controls
            If TypeOf ctl Is TabControl Then
                For Each tabcontrol As TabPage In CType(ctl, TabControl).TabPages
                    For Each ctl2 As Control In tabcontrol.Controls
                        If TypeOf ctl2 Is TextBox Then

                            CType(ctl2, TextBox).ReadOnly = Not bool

                            If CType(ctl2, TextBox).Name.ToLower.Contains("status") Then
                                If CType(ctl2, TextBox).Text.ToLower <> "draft" Or bool = False Then
                                    isDraft = False
                                End If
                            End If

                        ElseIf TypeOf ctl2 Is GroupBox Then
                            For Each ctl3 As Control In CType(ctl2, GroupBox).Controls
                                If TypeOf ctl3 Is TextBox And Not ctl3.Name.ToLower.Contains("status") Then
                                    CType(ctl3, TextBox).ReadOnly = Not bool
                                ElseIf TypeOf ctl3 IsNot Label Then
                                    ctl3.Enabled = bool
                                End If
                            Next
                        ElseIf TypeOf ctl2 Is Button Then
                            If CType(ctl2, Button).Name.ToLower.Contains("draft") Then
                                CType(ctl2, Button).Enabled = isDraft
                            End If
                        End If


                    Next
                Next
            ElseIf TypeOf ctl Is Button Then
                If CType(ctl, Button).Name.ToLower.Contains("draft") Then
                    CType(ctl, Button).Enabled = isDraft
                End If
            End If
        Next

    End Sub

    Public tooltip As ToolTip
    Public Function Form_validation(form As Form)
        Dim isDraft As Boolean = True
        SubClassWindow(form.Handle.ToInt32)
        If tooltip IsNot Nothing Then
            tooltip.Dispose()
        End If
        tooltip = New ToolTip
        tooltip.IsBalloon = True

        For Each ctl As Control In form.Controls
            If TypeOf ctl Is TabControl Then
                For Each tabcontrol As TabPage In CType(ctl, TabControl).TabPages
                    For Each ctl2 As Control In tabcontrol.Controls
                        If TypeOf ctl2 Is TextBox Then

                            If CType(ctl2, TextBox).Tag IsNot Nothing Then
                                If CType(ctl2, TextBox).Tag.ToString.ToLower.Contains("required") And CType(ctl2, TextBox).Text = "" Then
                                    'MsgBox("Please fill up the required fields.")

                                    'CType(ctl2, TextBox)
                                    tooltip.Show(String.Empty, CType(ctl2, TextBox), 0)
                                    tooltip.Show("Please fill up the required fields.", CType(ctl2, TextBox))
                                    CType(ctl2, TextBox).Focus()

                                    CType(ctl, TabControl).SelectedTab = tabcontrol

                                    RemoveHandler CType(ctl2, TextBox).KeyDown, AddressOf remove_tooltip
                                    AddHandler CType(ctl2, TextBox).KeyDown, AddressOf remove_tooltip

                                    Return False

                                End If
                            End If

                        ElseIf TypeOf ctl2 Is GroupBox Then
                            For Each ctl3 As Control In CType(ctl2, GroupBox).Controls
                                If TypeOf ctl3 Is TextBox Then
                                    If CType(ctl3, TextBox).Tag IsNot Nothing Then
                                        If CType(ctl3, TextBox).Text = "" And CType(ctl3, TextBox).Tag.ToString.ToLower.Contains("required") Then
                                            'MsgBox("Please fill up the required fields.")
                                            tooltip.Show(String.Empty, CType(ctl3, TextBox), 0)
                                            tooltip.Show("Please fill up the required fields.", CType(ctl3, TextBox))
                                            CType(ctl3, TextBox).Focus()
                                            CType(ctl, TabControl).SelectedTab = tabcontrol
                                            RemoveHandler CType(ctl3, TextBox).KeyDown, AddressOf remove_tooltip
                                            AddHandler CType(ctl3, TextBox).KeyDown, AddressOf remove_tooltip
                                            Return False

                                        End If
                                    End If
                                ElseIf TypeOf ctl3 Is ComboBox Then
                                    If CType(ctl3, ComboBox).Tag IsNot Nothing Then
                                        If CType(ctl3, ComboBox).Tag.ToString.ToLower.Contains("required") And CType(ctl3, ComboBox).SelectedValue = 0 Then
                                            tooltip.Show(String.Empty, CType(ctl3, ComboBox), 0)
                                            tooltip.Show("Please fill up the required fields.", CType(ctl3, ComboBox))
                                            CType(ctl3, ComboBox).Focus()

                                            CType(ctl, TabControl).SelectedTab = tabcontrol

                                            RemoveHandler CType(ctl3, ComboBox).SelectedIndexChanged, AddressOf remove_tooltip
                                            AddHandler CType(ctl3, ComboBox).SelectedIndexChanged, AddressOf remove_tooltip
                                            Return False
                                        End If
                                    End If

                                End If
                            Next
                        ElseIf TypeOf ctl2 Is ComboBox Then
                            If CType(ctl2, ComboBox).Tag IsNot Nothing Then
                                If CType(ctl2, ComboBox).Tag.ToString.ToLower.Contains("required") And CType(ctl2, ComboBox).SelectedValue = 0 Then
                                    tooltip.Show(String.Empty, CType(ctl2, ComboBox), 0)
                                    tooltip.Show("Please fill up the required fields.", CType(ctl2, ComboBox))
                                    CType(ctl2, ComboBox).Focus()

                                    CType(ctl, TabControl).SelectedTab = tabcontrol

                                    RemoveHandler CType(ctl2, ComboBox).SelectedIndexChanged, AddressOf remove_tooltip
                                    AddHandler CType(ctl2, ComboBox).SelectedIndexChanged, AddressOf remove_tooltip
                                    Return False
                                End If
                            End If

                        End If

                    Next
                Next
            ElseIf TypeOf ctl Is Button Then
                If CType(ctl, Button).Name.ToLower.Contains("draft") Then

                End If
            End If
        Next
        Return True
    End Function

    Public Function remove_tooltip()
        tooltip.Dispose()
    End Function




#Region "Set Dialog Form Properties: Para Dili na mag set manual Property sa Add Form, View Form og Edit Form"
    Public Function FormDiaologProperty(ByVal DialogForm As Form, ByVal Optional Title As String = "") As Form
        DialogForm.StartPosition = FormStartPosition.CenterScreen
        DialogForm.MinimizeBox = True
        DialogForm.MaximizeBox = False
        DialogForm.ShowIcon = False
        DialogForm.FormBorderStyle = FormBorderStyle.FixedSingle
        If Title <> "" Then
            DialogForm.Text = Title
        End If

        Return DialogForm
    End Function
#End Region

#Region "Message Box Questions: Para sa Save Update og Delete"
    Public Function SaveasDraftConfirmGlobal()
        If MsgBox("Do you want to save as draft the record?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function SaveConfirmGlobal()
        If MsgBox("Do you want to save the new record?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateConfirmGlobal(Optional ByVal RecordName As String = "")
        If MsgBox("Do you want to save changes to " + RecordName + "?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function DeleteConfirmGlobal()
        If MsgBox("Do you want to delete this record?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ReactivateConfirmGlobal()
        If MsgBox("Do you want to reactivate this record?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function DeactivateConfirmGlobal()
        If MsgBox("Do you want to deactivate this record?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Record Messages: After ma Save, Update, Delete, Reactivate, Deactivate, Suspend og Reinstate"

    Public Function SaveMessageGlobal()
        MsgBox("Successfully saved.", MsgBoxStyle.Information, "Information")
        Return True
    End Function

    Public Function SaveasDraftMessageGlobal()
        MsgBox("Successfully saved as draft.", MsgBoxStyle.Information, "Information")
        Return True
    End Function

    Public Function DeleteMessageGlobal()
        MsgBox("Successfully removed.", MsgBoxStyle.Information, "Information")
        Return True
    End Function

    Public Function UpdateMessageGlobal()
        MsgBox("Successfully updated.", MsgBoxStyle.Information, "Information")
        Return True
    End Function

    Public Function ReactivateMessageGlobal()
        MsgBox("Successfully reactivated.", MsgBoxStyle.Information, "Information")
        Return True
    End Function

    Public Function DeactivateMessageGlobal()
        MsgBox("Successfully deactivated.", MsgBoxStyle.Information, "Information")
        Return True
    End Function

    Public Function SuspendMessageGlobal()
        MsgBox("Successfully suspended", MsgBoxStyle.Information, "Information")
        Return True
    End Function

#End Region




#Region "Global_DateTimePickerFormat"
    Public Sub Global_DateTimePickerFormat(ByVal Form As Form)

        Dim DateTimePicker = GetAllControls(Form).OfType(Of DateTimePicker)().ToList()
        For Each item As DateTimePicker In DateTimePicker
            If item.Tag Is Nothing Then
                AddHandler item.ValueChanged, AddressOf DateTimePicker_ValueChanged
                item.Format = DateTimePickerFormat.Custom
                item.CustomFormat = "MM/dd/yyyy"
            End If
        Next

    End Sub

    Private Function GetAllControls(control As Control) As IEnumerable(Of Control)
        Dim controls = control.Controls.Cast(Of Control)()
        Return controls.SelectMany(Function(ctrl) GetAllControls(ctrl)).Concat(controls)
    End Function

    Private Sub DateTimePicker_ValueChanged(sender As Object, e As EventArgs)
        SendKeys.Send(".")
    End Sub

#End Region


#Region "No record/s exist in this view"
    Public Sub Global_PaintLabelNoRecords(ByVal sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs)
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = TryCast(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        If view.RowCount <> 0 Then
            Exit Sub
        End If
        Dim drawFormat As New StringFormat()
        drawFormat.LineAlignment = StringAlignment.Center
        drawFormat.Alignment = drawFormat.LineAlignment
        e.Graphics.DrawString("No record/s exist in this view", e.Appearance.Font, SystemBrushes.ControlDark, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat)
    End Sub
#End Region

#Region "HideColumns"
    Public Function Global_HideColumnsMethod(ByVal columncollection As Columns.GridColumnCollection)
        If _list.Count > 0 Then
            For Each column As Columns.GridColumn In columncollection
                For Each li In _list
                    If column.FieldName = li Then
                        column.Visible = False
                    End If
                Next
            Next
        End If
        Return True
    End Function
#End Region

#Region "Replace Underscore Columns"
    Dim _list As New List(Of String)
    Public Function Global_ReplaceUnderscoreColumns(ByVal dt As DataTable) As DataTable
        _list.Clear()
        If dt Is Nothing Then
            Exit Function
        End If
        For Each dc As DataColumn In dt.Columns
            If dc.ColumnName.Substring(0, 1) = "_" Then
                dc.ColumnName = Replace(dc.ColumnName, "_", "")
                _list.Add(dc.ColumnName)
            End If
        Next
        Return dt
    End Function
#End Region


#Region "Session DateFrom & DateTo"
    Public global_dateFrom As DateTime
    Public global_dateTo As DateTime
#End Region


End Module