﻿Module WinProc

    'Retrieves information about the specified window
    'Declare Function GetWindowLong Lib "user32.dll" Alias _
    '   "GetWindowLongA" (ByVal hwnd As Int32,
    '   ByVal nIndex As Int32) As Int32

    'Passes message information to the specified window
    'procedure
    Declare Function CallWindowProc Lib "user32.dll" Alias _
       "CallWindowProcA" (ByVal lpPrevWndFunc As Int32,
          ByVal hwnd As Int32, ByVal msg As Int32,
          ByVal wParam As Int32, ByVal lParam As Int32) As Int32


    'Changes an attribute of the specified window
    Declare Function SetWindowLong Lib "USER32.DLL" Alias _
       "SetWindowLongA" (ByVal hwnd As Integer,
          ByVal attr As Integer,
          ByVal lVal As SubClassProcDelegate) As Integer

    Declare Function SetWindowLong Lib "USER32.DLL" Alias _
       "SetWindowLongA" (ByVal hwnd As Integer,
          ByVal attr As Integer,
         ByVal dwnewLong As Integer) As Integer


    ' Sets a new address for the window procedure
    Const GWL_WNDPROC As Short = (-4)

    'This message is sent to a window when the user chooses
    'a command from the window menu, formerly known as the
    'system or control menu, or when the user chooses the
    'maximize button or the minimize button.
    Private Const WM_SYSCOMMAND As Short = &H112S

    Public WndProcOld As Int32   'Old Window Procedure

    'All messages are sent to the WndProc method after
    'getting filtered through the PreProcessMessage method
    Public Function WindProc(ByVal hwnd As Int32,
       ByVal wMsg As Int32, ByVal wParam As Int32,
       ByVal lParam As Int32) As Int32

        'Do nothing when any system buttons are pressed
        'If wMsg = WM_SYSCOMMAND Then Exit Function
        Console.WriteLine(wMsg)
        If wMsg = 161 Or wMsg = 78 Then
            If tooltip IsNot Nothing Then
                tooltip.Dispose()
            End If
        End If

        'Override old Window method
        Return CallWindowProc(WndProcOld, hwnd, wMsg,
           wParam, lParam)

    End Function

    'Function to override normal activity
    Delegate Function SubClassProcDelegate(ByVal hwnd As Integer,
       ByVal msg As Integer, ByVal wParam As Integer,
       ByVal lParam As Integer) As Integer

    'Subclass ( override ) procedure

    Private newWndProc As SubClassProcDelegate = New SubClassProcDelegate(AddressOf WindProc)
    Public _hwnd As Integer

    Sub SubClassWindow(ByVal hwnd As Integer)

        If _hwnd <> hwnd Then
            'Replace function
            _hwnd = hwnd
            WndProcOld = SetWindowLong(hwnd, GWL_WNDPROC,
            newWndProc)
        End If

    End Sub

End Module