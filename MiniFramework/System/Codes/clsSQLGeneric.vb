﻿Imports Npgsql
Imports System.Drawing.Printing
Imports System.Configuration

Public Class clsSQLGeneric
    'Returns Datatable
    Public Function customCommandDataTable(ByVal command As String) As DataTable

        If cn.State = ConnectionState.Open Then cn.Close()
        Try
            cn.Open()
        Catch
            MsgBox(String.Format("Unable to connect to server, try again later"), MsgBoxStyle.Information, "Connection")
            Exit Function
        End Try

        Dim exec As String
        exec = command
        Dim cmd As New NpgsqlCommand(exec, cn)
        cmd.CommandTimeout = 120000
        Dim SA As New NpgsqlDataAdapter(cmd)
        Dim DS As New DataSet("custom_command")

        Try
            SA.Fill(DS, "custom_command")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return DS.Tables("custom_command")
        cn.Dispose()
        cn.Close()

    End Function

    'Returns Boolean 
    Public Function customCommand(ByVal command As String) As Boolean
        Dim retval As Boolean = False
        If cn.State = ConnectionState.Open Then cn.Close()
        cn.Open()
        Dim exec As String = command

        Dim cmd As New NpgsqlCommand(exec, cn)
        Try
            cmd.ExecuteNonQuery()
            retval = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return retval
        cn.Dispose()
    End Function

    Public Function customCommandObject(ByVal command As String, Optional ByVal columnIndex As Integer = 0) As Object
        Dim retval As Object = Nothing
        If cn.State = ConnectionState.Open Then cn.Close()
        cn.Open()

        Dim exec As String = command

        Dim cmd As New NpgsqlCommand(exec, cn)

        Dim myReader As NpgsqlDataReader
        myReader = cmd.ExecuteReader

        If Not myReader.HasRows Then
            retval = Nothing
        Else
            While myReader.Read()
                Try
                    retval = myReader(columnIndex)
                Catch ex As Exception
                    retval = Nothing
                    MsgBox(ex.Message)
                End Try
            End While
        End If

        Return retval
        cn.Dispose()
        cn.Close()
    End Function


End Class

'SQL Module
Module SQLModule
    Private SQL As New clsSQLGeneric
    Private transact As NpgsqlTransaction
    Dim cmd As NpgsqlCommand

    Public cn As New NpgsqlConnection(ConfigurationManager.ConnectionStrings("dbconnection").ConnectionString)

    'Public IP_Add As String = "192.168.3.223"
    Public global_prnt_settings As New PrinterSettings

    Public Function DataSource(ByVal command As String) As DataTable
        Return SQL.customCommandDataTable(command)
    End Function

    Public Function DataSource(ByVal command As String, ByVal transactional As Boolean) As DataTable

        'Return SQL.customCommandDataTable(command)

        'If cn.State = ConnectionState.Open Then cn.Close()
        'Try
        '    cn.Open()
        'Catch
        '    MsgBox(String.Format("Disconnected from the server, try again later"), MsgBoxStyle.Information, "Connection")
        'End Try


        cmd.CommandText = command
        cmd.Connection = cn
        'Dim cmd As New NpgsqlCommand(exec, cn)
        Dim SA As New NpgsqlDataAdapter(cmd)
        Dim DS As New DataSet("custom_command")


        SA.Fill(DS, "custom_command")

        Return DS.Tables("custom_command")

        'cn.Dispose()
        'cn.Close()

    End Function

    'SQL Command
    Public Function SQLCommand(ByVal command As String) As Boolean
        Return SQL.customCommand(command)
    End Function

    'get DataObject
    Public Function DataObject(ByVal command As String, Optional ByVal column As Integer = 0) As Object
        DataObject = SQL.customCommandObject(command, column)
    End Function

    Public Function queryCustom(ByVal sql As String, ByVal dic As Dictionary(Of String, Object))
        Dim return_value As String = ""
        cmd.Connection = cn
        'cmd.ExecuteNonQuery()

        Dim reader As NpgsqlDataReader
        reader = cmd.ExecuteReader()
        While (reader.Read())
            return_value = reader.GetString(0)
            'return_value = reader.GetInt32(0)
        End While
        reader.Dispose()
        Return return_value

    End Function

    Public Sub InsertBiometricLog(ByVal col As String, ByVal list As List(Of Dictionary(Of String, Object)), ByVal table As String, ByVal pk As String, Optional ByVal returnType As String = "integer")

        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As New List(Of Object)
        Dim i As Integer = 0

        Dim stringquery As String = "COPY """ & table & """ (" & col & ") FROM STDIN (FORMAT BINARY)"
        Using writer = cn.BeginBinaryImport(stringquery)

            For Each dic As Dictionary(Of String, Object) In list

                If i = 5512 Then
                    Console.Write("1")
                End If

                writer.StartRow()
                writer.Write(dic.Item("EmployeeBiometricID" & i), NpgsqlTypes.NpgsqlDbType.Bigint)
                writer.Write(dic.Item("TimeLogs" & i), NpgsqlTypes.NpgsqlDbType.Timestamp)
                writer.Write(dic.Item("ModeID" & i), NpgsqlTypes.NpgsqlDbType.Integer)
                writer.Write(dic.Item("ModeType" & i), NpgsqlTypes.NpgsqlDbType.Varchar)
                writer.Write(dic.Item("DeviceID" & i), NpgsqlTypes.NpgsqlDbType.Integer)
                writer.Write(dic.Item("IpAddress" & i), NpgsqlTypes.NpgsqlDbType.Varchar)
                writer.Write(dic.Item("DateFrom" & i), NpgsqlTypes.NpgsqlDbType.Date)
                writer.Write(dic.Item("DateTo" & i), NpgsqlTypes.NpgsqlDbType.Date)
                writer.Write(dic.Item("DeptID" & i), NpgsqlTypes.NpgsqlDbType.Bigint)
                i = i + 1

            Next

        End Using


    End Sub

    Public Sub InsertAttendance(ByVal Data As DataTable)

        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As New List(Of Object)
        Dim i As Integer = 0

        Dim stringquery As String = "COPY ""Attendance"" (""PartyID"",""Date"",""CheckIn"",""BreakOut"",""BreakIn"",""CheckOut"",""OvertimeOut"",""OvertimeIn"",""Overtime"",""Late"",""Undertime"",""ShiftCheckIn"",""ShiftBreakOut"",""ShiftBreakIn"",""ShiftCheckOut"",""DepartmentID"",""Restday"",""Modified"",""NightDiff"",""ApprovedLateUT"") FROM STDIN (FORMAT BINARY)"
        'Dim stringquery As String = "COPY ""Attendance"" (""PartyID"",""Date"",""CheckIn"",""BreakOut"") FROM STDIN (FORMAT BINARY)"
        Using writer = cn.BeginBinaryImport(stringquery)

            For Each row As DataRow In Data.Rows

                writer.StartRow()
                If Not IsDBNull(row("PartyID")) Then
                    writer.Write(row("PartyID"), NpgsqlTypes.NpgsqlDbType.Bigint)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("Date")) Then
                    writer.Write(row("Date"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("CheckIn")) Then
                    writer.Write(row("CheckIn"), NpgsqlTypes.NpgsqlDbType.Timestamp)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("BreakOut")) Then
                    writer.Write(row("BreakOut"), NpgsqlTypes.NpgsqlDbType.Timestamp)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("BreakIn")) Then
                    writer.Write(row("BreakIn"), NpgsqlTypes.NpgsqlDbType.Timestamp)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("CheckOut")) Then
                    writer.Write(row("CheckOut"), NpgsqlTypes.NpgsqlDbType.Timestamp)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("OvertimeOut")) Then
                    writer.Write(row("OvertimeOut"), NpgsqlTypes.NpgsqlDbType.Timestamp)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("OvertimeIn")) Then
                    writer.Write(row("OvertimeIn"), NpgsqlTypes.NpgsqlDbType.Timestamp)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("Overtime")) Then
                    writer.Write(row("Overtime"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("Late")) Then
                    writer.Write(row("Late"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("Undertime")) Then
                    writer.Write(row("Undertime"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("ShiftCheckIn")) Then
                    writer.Write(row("ShiftCheckIn"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("ShiftBreakOut")) Then
                    writer.Write(row("ShiftBreakOut"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("ShiftBreakIn")) Then
                    writer.Write(row("ShiftBreakIn"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("ShiftCheckOut")) Then
                    writer.Write(row("ShiftCheckOut"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("DepartmentID")) Then
                    writer.Write(row("DepartmentID"), NpgsqlTypes.NpgsqlDbType.Integer)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("Restday")) Then
                    writer.Write(row("Restday"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("Modified")) Then
                    writer.Write(row("Modified"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("NightDiff")) Then
                    writer.Write(row("NightDiff"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

                If Not IsDBNull(row("ApprovedLateUT")) Then
                    writer.Write(row("ApprovedLateUT"), NpgsqlTypes.NpgsqlDbType.Varchar)
                Else
                    writer.WriteNull()
                End If

            Next

        End Using


    End Sub



    Public Function queryInsertBulk(ByVal col As String, ByVal list As List(Of Dictionary(Of String, Object)), ByVal table As String, ByVal pk As String, Optional ByVal returnType As String = "integer")
        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As New List(Of Object)

        Dim query As String = ""

        Dim str_val As String = ""

        For Each dic As Dictionary(Of String, Object) In list

            For i As Integer = 0 To dic.Count - 1
                If i = 0 Then
                    columns = """" & dic.Keys(i) & """"
                    parameterize = "@" & dic.Keys(i)
                Else
                    columns &= ",""" & dic.Keys(i) & """"
                    parameterize &= "," & "@" & dic.Keys(i)

                End If

                If IsNothing(dic.Values(i)) Then
                    cmd.Parameters.AddWithValue(dic.Keys(i), DBNull.Value)
                Else
                    cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))
                End If
                cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))

            Next

            If str_val = "" Then
                str_val = "(" & parameterize & ")"
            Else
                str_val &= ",(" & parameterize & ")"
            End If

            parameterize = ""

        Next

        query = "INSERT INTO """ & table & """(" & col & ") VALUES " & str_val & " RETURNING """ & pk & """"

        cmd.CommandText = query
        cmd.Connection = cn
        'cmd.ExecuteNonQuery()

        Dim reader As NpgsqlDataReader

        reader = cmd.ExecuteReader()


        While (reader.Read())
            If returnType = "integer" Then
                return_value.Add(reader.GetInt32(0))
            ElseIf returnType = "String" Then
                return_value.Add(reader.GetString(0))
            End If
        End While
        reader.Dispose()
        Return return_value
    End Function
    'Execute Queries for Insert, Update, Delete
    Public Function queryInsert(ByVal dic As Dictionary(Of String, Object), ByVal table As String, ByVal pk As String, Optional ByVal returnType As String = "Integer")
        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As Object = ""

        Dim query As String = ""

        For i As Integer = 0 To dic.Count - 1
            If i = 0 Then
                columns = """" & dic.Keys(i) & """"
                parameterize = "@" & dic.Keys(i)
            Else
                columns &= ",""" & dic.Keys(i) & """"
                parameterize &= "," & "@" & dic.Keys(i)

            End If

            If IsNothing(dic.Values(i)) Then
                cmd.Parameters.AddWithValue(dic.Keys(i), DBNull.Value)
            Else
                cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))
            End If
            cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))

        Next

        query = "INSERT INTO """ & table & """(" & columns & ") VALUES(" & parameterize & ") RETURNING """ & pk & """"

        cmd.CommandText = query
        cmd.Connection = cn
        'cmd.ExecuteNonQuery()

        Dim reader As NpgsqlDataReader

        reader = cmd.ExecuteReader()

        While (reader.Read())
            If returnType = "Integer" Then
                return_value = reader.GetInt32(0)
            ElseIf returnType = "String" Then
                return_value = reader.GetString(0)
            End If
        End While
        reader.Dispose()
        Return return_value
    End Function

    Public Function queryUpdate(ByVal obj As Object, ByVal dic As Dictionary(Of String, Object), ByVal table As String, ByVal pk As String, Optional ByVal returnType As String = "Integer")
        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As Object = ""
        Dim query As String = ""

        For i As Integer = 0 To dic.Count - 1
            If i = 0 Then
                columns = """" & dic.Keys(i) & """=@" & dic.Keys(i)
            Else
                columns &= ",""" & dic.Keys(i) & """=@" & dic.Keys(i)
            End If

            If IsNothing(dic.Values(i)) Then
                cmd.Parameters.AddWithValue(dic.Keys(i), DBNull.Value)
            Else
                cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))
            End If
            cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))

        Next

        query = "UPDATE """ & table & """ Set " & columns & " WHERE """ & obj.name & """ = '" & obj.value & "' RETURNING """ & pk & """"

        cmd.CommandText = query
        cmd.Connection = cn
        'cmd.ExecuteNonQuery()

        Dim reader As NpgsqlDataReader

        reader = cmd.ExecuteReader()

        While (reader.Read())
            If returnType = "Integer" Then
                return_value = reader.GetInt32(0)
            ElseIf returnType = "string" Then
                return_value = reader.GetString(0)
            End If

        End While
        reader.Dispose()
        Return return_value
    End Function


    Public Function queryUpdateCondition(ByVal obj As List(Of Object), ByVal dic As Dictionary(Of String, Object), ByVal table As String, ByVal pk As String, Optional ByVal returnType As String = "integer")
        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As Object = ""
        Dim query As String = ""

        For i As Integer = 0 To dic.Count - 1
            Console.WriteLine(dic.Keys(i).ToString.Substring(0, 1))
            If dic.Keys(i).ToString.Substring(0, 1) = "_" Then
                Dim kkeys = dic.Keys(i).ToString.Replace("_", "")
                Dim rep As String = String.Empty
                For z As Integer = 0 To kkeys.Count - 1
                    If Not IsNumeric(kkeys.Substring(z, 1)) Then
                        rep += kkeys.Substring(z, 1)
                    End If
                Next
                kkeys = rep

                If i = 0 Then
                    columns = """" & kkeys & """=@" & dic.Keys(i)
                Else
                    columns &= ",""" & kkeys & """=@" & dic.Keys(i)
                End If

            Else
                If i = 0 Then
                    columns = """" & dic.Keys(i) & """=@" & dic.Keys(i)
                Else
                    columns &= ",""" & dic.Keys(i) & """=@" & dic.Keys(i)
                End If
            End If



            If IsNothing(dic.Values(i)) Then
                cmd.Parameters.AddWithValue(dic.Keys(i), DBNull.Value)
            Else
                cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))
            End If
            cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))

        Next

        For Each item As Object In obj
            parameterize &= " " & item.condition & " """ & item.name & """ = '" & item.value & "'"
        Next

        query = "UPDATE """ & table & """ SET " & columns & " WHERE " & parameterize & " RETURNING """ & pk & """"

        cmd.CommandText = query
        cmd.Connection = cn
        'cmd.ExecuteNonQuery()

        Dim reader As NpgsqlDataReader

        reader = cmd.ExecuteReader()

        While (reader.Read())
            If returnType = "integer" Then
                return_value = reader.GetInt32(0)
            ElseIf returnType = "string" Then
                return_value = reader.GetString(0)
            End If

        End While
        reader.Dispose()
        Return return_value
    End Function

    Public Function queryUpdateCondition(ByVal where As String, ByVal dic As Dictionary(Of String, Object), ByVal table As String, ByVal pk As String, Optional ByVal returnType As String = "integer")
        Dim columns As String = ""
        Dim values As String = ""
        Dim parameterize As String = ""
        Dim return_value As Object = ""
        Dim query As String = ""

        For i As Integer = 0 To dic.Count - 1
            Console.WriteLine(dic.Keys(i).ToString.Substring(0, 1))
            If dic.Keys(i).ToString.Substring(0, 1) = "_" Then
                Dim kkeys = dic.Keys(i).ToString.Replace("_", "")
                Dim rep As String = String.Empty
                For z As Integer = 0 To kkeys.Count - 1
                    If Not IsNumeric(kkeys.Substring(z, 1)) Then
                        rep += kkeys.Substring(z, 1)
                    End If
                Next
                kkeys = rep

                If i = 0 Then
                    columns = """" & kkeys & """=@" & dic.Keys(i)
                Else
                    columns &= ",""" & kkeys & """=@" & dic.Keys(i)
                End If

            Else
                If i = 0 Then
                    columns = """" & dic.Keys(i) & """=@" & dic.Keys(i)
                Else
                    columns &= ",""" & dic.Keys(i) & """=@" & dic.Keys(i)
                End If
            End If

            If IsNothing(dic.Values(i)) Then
                cmd.Parameters.AddWithValue(dic.Keys(i), DBNull.Value)
            Else
                cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))
            End If
            cmd.Parameters.AddWithValue(dic.Keys(i), dic.Values(i))

        Next

        query = "UPDATE """ & table & """ SET " & columns & " WHERE " & where & " RETURNING """ & pk & """"

        cmd.CommandText = query
        cmd.Connection = cn
        'cmd.ExecuteNonQuery()

        Dim reader As NpgsqlDataReader

        reader = cmd.ExecuteReader()

        While (reader.Read())
            If returnType = "integer" Then
                return_value = reader.GetInt32(0)
            ElseIf returnType = "string" Then
                return_value = reader.GetString(0)
            End If

        End While
        reader.Dispose()
        Return return_value
    End Function


    'Execute Queries for Select return datatable for transaction
    Function select_query(ByVal sql As String) As DataTable
        cmd.CommandText = sql
        cmd.Connection = cn


        Dim SA As New NpgsqlDataAdapter(cmd)
        Dim DS As New DataSet("custom_command")

        Try
            SA.Fill(DS, "custom_command")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return DS.Tables("custom_command")


    End Function

    'Execute Queries for Insert, Update, Delete
    Sub RunQuery(ByVal sql As String)
        cmd.CommandText = sql
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
    End Sub

    'Execute Queries for Insert, Update, Delete | Get rows affected
    Function RunQuery_Row(ByVal sql As String) As Integer
        cmd.CommandText = sql
        cmd.Connection = cn
        Return cmd.ExecuteNonQuery()
    End Function

    'Execute Queries for Insert, Update, Delete | Get Data
    Function RunQuery_Data(ByVal sql As String) As Integer
        cmd.CommandText = sql
        cmd.Connection = cn
        Return cmd.ExecuteScalar()
    End Function

    'Begin the Transaction by opening the connection
    Public Sub StartTransaction()
        If cn.State = ConnectionState.Open Then
            cn.Close()
        End If
        If cn.State = ConnectionState.Closed Then
            cn.Open()
        End If
        cmd = New NpgsqlCommand()
        transact = cn.BeginTransaction
        cmd.Transaction = transact
    End Sub

    'End the Transaction by closing the connection
    Public Sub EndTransaction()
        cn.Close()
    End Sub

    Public Sub commitQuery()
        transact.Commit()
    End Sub

    Public Sub rollbackQuery()
        Try
            transact.Rollback()
        Catch ex As Exception
        End Try
    End Sub

    Public Function ReplaceText(ByVal str As String)
        Dim new_str As String = str.Replace("'", "`")
        Return new_str
    End Function

    'Public Sub SavePhoto(ByVal sql As String, ByVal picturePath As String)
    '    Try
    '        Dim rawData() As Byte
    '        Dim fs As FileStream
    '        fs = New FileStream(picturePath, FileMode.Open, FileAccess.Read)

    '        rawData = New Byte(fs.Length) {}
    '        fs.Read(rawData, 0, fs.Length)
    '        fs.Close()

    '        If cn.State = ConnectionState.Open Then
    '            cn.Close()
    '        End If
    '        cn.Open()

    '        Dim cmd As New NpgsqlCommand(sql, cn)
    '        cmd.Connection = cn
    '        cmd.CommandText = sql
    '        cmd.Parameters.Remove(New NpgsqlParameter("?bin_data", rawData))
    '        cmd.Parameters.Add(New NpgsqlParameter("?bin_data", rawData))
    '        cmd.ExecuteNonQuery()
    '        cn.Close()
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '        cn.Close()
    '    End Try
    'End Sub

    'Public Sub RetrieveImage(ByVal sql As String, ByVal Photo As PictureBox, ByVal Field As String)
    '    Dim arrImage() As Byte
    '    Try
    '        If cn.State = ConnectionState.Open Then
    '            cn.Close()
    '        End If
    '        cn.Open()
    '        Dim cmd As New NpgsqlCommand(sql, cn)
    '        Dim myReader As NpgsqlDataReader
    '        cmd.Connection = cn
    '        cmd.CommandText = sql
    '        myReader = cmd.ExecuteReader()
    '        myReader.Read()
    '        arrImage = myReader.Item(Field)
    '        Dim mstream As New System.IO.MemoryStream(arrImage)
    '        Photo.Image = Image.FromStream(mstream)
    '    Catch EX As Exception
    '        Photo.Image = Nothing
    '        cn.Close()
    '        Exit Sub
    '    End Try

    '    cn.Close()
    'End Sub

    'Public Sub RetrieveImageReport(ByVal sql As String, ByVal Photo As DataDynamics.ActiveReports.Picture, ByVal Field As String)
    '    Dim arrImage() As Byte
    '    Try
    '        If cn.State = ConnectionState.Open Then
    '            cn.Close()
    '        End If
    '        cn.Open()
    '        Dim cmd As New MySqlCommand(sql, cn)
    '        Dim myReader As MySqlDataReader
    '        cmd.Connection = cn
    '        cmd.CommandText = sql
    '        myReader = cmd.ExecuteReader()
    '        myReader.Read()
    '        arrImage = myReader.Item(Field)
    '        Dim mstream As New System.IO.MemoryStream(arrImage)
    '        Photo.Image = Image.FromStream(mstream)
    '    Catch EX As Exception
    '        Photo.Image = Nothing
    '        cn.Close()
    '        Exit Sub
    '    End Try

    '    cn.Close()
    'End Sub
End Module

