﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class PersonSetupFactory


    Public Function ListForm() As Form
        Dim frm As New PersonSetupList()
        Return frm
    End Function

    Public Function AddForm(ByVal List As Object)
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim presenter As PersonSetupListPresenter = CType(List, PersonSetupListPresenter)
            presenter.SelectedID = 0
            Dim app As New PersonSetupRecord(presenter)
            app.chkEdit.Enabled = True
            app.btnSaveDraft.Enabled = True
            app.ShowDialog()
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Sub EditForm(ByVal List As Object, ByVal dr As DataRowView)
        Try
            If dr IsNot Nothing Then
                Cursor.Current = Cursors.WaitCursor
                Dim presenter As PersonSetupListPresenter = CType(List, PersonSetupListPresenter)
                presenter.SelectedID = dr("PersonID").ToString
                Dim frm As New PersonSetupRecord(presenter)
                frm.mode = "edit"
                frm.ShowDialog()
                Cursor.Current = Cursors.Default
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub Delete(ByVal List As Object, ByVal dr As DataRowView)
        Try
            Dim presenter As PersonSetupListPresenter = CType(List, PersonSetupListPresenter)
            Dim label As String
            If dr IsNot Nothing Then
                label = dr("Status").ToString()
                If label <> "Draft" Then
                    MsgBox("Cannot Delete this Record, Data is in used", MsgBoxStyle.Exclamation, "Delete Record")
                Else
                    presenter.SelectedID = dr("PersonID").ToString
                    If MessageBox.Show("Are you sure you want to Delete", "Delete Record", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        If presenter.delete() Then
                            presenter.refresh()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Public Sub Deactivate(ByVal List As Object, ByVal dr As DataRowView)
        Try
            Cursor.Current = Cursors.WaitCursor
            If dr Is Nothing Then
                Exit Sub
            End If
            Dim presenter As PersonSetupListPresenter = CType(List, PersonSetupListPresenter)
            If MessageBox.Show("Are you sure you want to Deactivate this Record?", "Confirm Deactivation", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) = DialogResult.Yes Then
                presenter.SelectedID = dr("PersonID").ToString
                presenter.Deactivate()
                presenter.refresh()

            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub Reactivate(ByVal List As Object, ByVal dr As DataRowView)
        Try
            Cursor.Current = Cursors.WaitCursor
            If dr Is Nothing Then
                Exit Sub
            End If
            Dim presenter As PersonSetupListPresenter = CType(List, PersonSetupListPresenter)
            If MessageBox.Show("Are you sure you want to Reactivate this Record?", "Confirm Reactivation", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) = DialogResult.Yes Then
                presenter.SelectedID = dr("PersonID").ToString
                presenter.Reactivate()
                presenter.refresh()

            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function Picture() As Form
        Dim presenter As PersonSetupCameraPresenter
        Dim frm As New PersonSetupCamera(presenter)
        frm.ShowDialog()
        Return frm
    End Function


    Public Function Report(ByVal List As Object)
        'Try
        Cursor.Current = Cursors.WaitCursor
            Dim presenter As JournalEntryVoucherListPresenter = CType(List, JournalEntryVoucherListPresenter)
            presenter.SelectedID = 0
            Dim app As New ReportRecord(presenter)
            app.mode = "add"
            app.chkEdit.Enabled = True
            app.ShowDialog()
            Cursor.Current = Cursors.Default
        'Catch ex As Exception
        '    MsgBox("Factory " + ex.Message)
        'End Try
    End Function

    Public Function JournalList() As Form
        Dim presenter As JournalEntryVoucherListPresenter
        Dim frm As New JournalList()
        'frm.ShowDialog()
        Return frm
    End Function

    Public Sub EditEntries(ByVal List As Object, ByVal dr As DataRowView)
        Try
            If dr IsNot Nothing Then
                Cursor.Current = Cursors.WaitCursor
                Dim presenter As JournalEntryVoucherListPresenter = CType(List, JournalEntryVoucherListPresenter)
                presenter.SelectedID = dr("JournalEntryMainID").ToString
                Dim frm As New JournalList()
                frm.mode = "edit"
                frm.ShowDialog()
                Cursor.Current = Cursors.Default
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Public Sub DeleteEntries(ByVal List As Object, ByVal dr As DataRowView)
        Try
            Dim presenter As JournalEntryVoucherListPresenter = CType(List, JournalEntryVoucherListPresenter)
            If dr IsNot Nothing Then
                presenter.SelectedID = dr("JournalEntryDetailID").ToString
                If MessageBox.Show("Are you sure you want to Delete", "Delete Record", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    If presenter.deleteEntries() Then
                        presenter.refresh()
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

End Class
