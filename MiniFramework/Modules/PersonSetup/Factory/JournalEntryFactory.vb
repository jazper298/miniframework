﻿Public Class JournalEntryFactory

    Public Function Report(ByVal List As Object)
        'Try
        Cursor.Current = Cursors.WaitCursor
        Dim presenter As JournalEntryVoucherListPresenter = CType(List, JournalEntryVoucherListPresenter)
        presenter.SelectedID = 0
        Dim app As New ReportRecord(presenter)
        app.mode = "add"
        app.chkEdit.Enabled = True
        app.ShowDialog()
        Cursor.Current = Cursors.Default
        'Catch ex As Exception
        '    MsgBox("Factory " + ex.Message)
        'End Try
    End Function

    Public Function JournalList() As Form
        Dim presenter As JournalEntryVoucherListPresenter
        Dim frm As New JournalList()
        'frm.ShowDialog()
        Return frm
    End Function

    Public Sub EditEntries(ByVal List As Object, ByVal dr As DataRowView)
        Try
            If dr IsNot Nothing Then
                Cursor.Current = Cursors.WaitCursor
                Dim presenter As JournalEntryVoucherListPresenter = CType(List, JournalEntryVoucherListPresenter)
                presenter.SelectedID = dr("JournalEntryMainID").ToString
                Dim frm As New ReportRecord(presenter)
                frm.mode = "edit"
                frm.ShowDialog()
                Cursor.Current = Cursors.Default
            End If
        Catch ex As Exception
        MsgBox("Factory" + ex.Message)
        End Try
    End Sub


    Public Sub DeleteEntries(ByVal List As Object, ByVal dr As DataRowView)
        Try
            Dim presenter As JournalEntryVoucherListPresenter = CType(List, JournalEntryVoucherListPresenter)
            If dr IsNot Nothing Then
                presenter.SelectedID = dr("JournalEntryMainID").ToString
                If MessageBox.Show("Are you sure you want to Delete", "Delete Record", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    If presenter.deleteEntries() Then
                        presenter.refresh()
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

End Class
