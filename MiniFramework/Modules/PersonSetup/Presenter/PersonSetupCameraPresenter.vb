﻿Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow
Imports AForge.Imaging
Imports System.IO
Imports System.Drawing

Public Class PersonSetupCameraPresenter
    Private _view As iPersonSetupCamera
    Private data As New PersonSetup
    Private model As New PersonSetupModel

    Public Sub New(ByVal view As iPersonSetupCamera, Optional refresh As Boolean = True)
        _view = view
    End Sub
    Property SelectedID
        Get
            Return _view.selectedID
        End Get
        Set(value)
            _view.selectedID = value
        End Set
    End Property
    Public Sub capture()
        data.Picture = _view.YourPicture
        data.Camera = _view.Camera

        _view.YourPicture = _view.Camera
    End Sub
    Public Sub save()

        'data.Dialog = _view.Dialog

        If _view.YourPicture Is Nothing Then
            MsgBox("please take your picture")
        Else
            _view.Dialog.DefaultExt = ".jpg"
            If _view.Dialog.ShowDialog = DialogResult.OK Then
                _view.YourPicture.Save(_view.Dialog.FileName, Imaging.ImageFormat.Jpeg)
                _view.WebCam.Stop()
            End If
        End If

        _view.YourPicture = Nothing

    End Sub

    Dim Camera1s As VideoCaptureDevice
    Dim bmp As Bitmap
    Dim start As Integer
    Public Sub startCam()

        data.WebCam = _view.WebCam
        Dim cameras As VideoCaptureDeviceForm = New VideoCaptureDeviceForm
        If cameras.ShowDialog() = DialogResult.OK Then
            _view.WebCam = cameras.VideoDevice
            AddHandler _view.WebCam.NewFrame, New NewFrameEventHandler(AddressOf Captured)
            _view.Camera = data.Camera
            _view.WebCam.Start()
            start = 1
        End If
    End Sub

    Public Sub Captured(obj As Object, e As NewFrameEventArgs)
        bmp = DirectCast(e.Frame.Clone(), Bitmap)
        data.Camera = DirectCast(e.Frame.Clone(), Bitmap)
        _view.Camera = data.Camera
    End Sub

    Public Sub closeCam()
        If start = 1 Then
            _view.WebCam.Stop()
        End If
    End Sub
End Class
