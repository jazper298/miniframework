﻿Public Class JournalEntryVoucherListPresenter

    Private _view As iJournalEntryList

    Private model As New JournalEntryModel

    Public Sub New(ByVal view As iJournalEntryList, Optional newList As Boolean = True)
        _view = view
        If newList Then
            refresh()
        End If
    End Sub
    Public Sub refresh()
        _view.dt = model.GetAllJournalRecord2()
    End Sub

    Property SelectedID
        Get
            Return _view.selectedID
        End Get
        Set(value)
            _view.selectedID = value
        End Set
    End Property

    Public Function deleteEntries()
        Dim dt As DataTable = model.GetJournalRecord(SelectedID)
        If dt.Rows.Count > 0 Then
            Return model.DeleteEntries(SelectedID)
        End If
        Return False
    End Function
End Class
