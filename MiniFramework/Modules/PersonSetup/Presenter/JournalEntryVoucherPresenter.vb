﻿Public Class JournalEntryVoucherPresenter
    Private _view As iJournalEntryVoucher
    Private data As New JournalEntryObject
    Private model As New JournalEntryModel


    Public Sub New(ByVal view As iJournalEntryVoucher, ByVal Optional refresh As Boolean = False)
        _view = view
    End Sub

    Public Sub onClick(ByVal ID As Integer)
        Dim dt As DataTable
        dt = model.GetAccountCode(ID)
        Try
            data.AccountTitleID = ID

            For Each rowobj In _view.JournalObj
                data.AccountCode = dt(0)("AccountCode")

                rowobj.AccountCode = data.AccountCode
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Dim dt As DataTable
    Public Sub editJournal(ByVal ID As Integer)
        dt = model.GetJournalRecord(ID)


        'Try
        '    data.JournalEntryMainID = ID
        '    For Each rowobj In _view.JournalObj
        '        data.ResponsibleCenter = dt(0)("ResponsibleCenter")
        '        data.AccountName = dt(0)("AccountName")
        '        data.Code = dt(0)("AccountCode")
        '        data.Amount = dt(0)("Amount")

        '        rowobj.ResponsibleCenter = data.ResponsibleCenter
        '        rowobj.AccountName = data.AccountName
        '        rowobj.Code = data.Code
        '        rowobj.Amount = data.Amount
        '    Next
        'Catch ex As Exception
        '    MsgBox("Edit editJournal " + ex.Message)
        'End Try

    End Sub

    Public Function getJournal(ByVal selectedID As Integer) As DataTable
        dt = model.GetJournalRecord(selectedID)
        Return dt
    End Function

    Public Sub saveJournal()
        Cursor.Current = Cursors.WaitCursor
        data.TransactionDate = _view._date
        'data.ResponsibleCenter = _view.JournalObj
        For Each rowobj In _view.JournalObj
            data.ResponsibleCenter = rowobj.ResponsibleCenter
            data.AccountName = rowobj.AccountName
            data.AccountCode = rowobj.AccountCode
            data.Debit = rowobj.Debit
            data.Credit = rowobj.Credit
            data.Type = rowobj.Type

            If data.Debit = Nothing Then
                rowobj.Type = "Credit"
                rowobj.Amount = data.Credit
            ElseIf data.Credit = Nothing Then
                rowobj.Type = "Debit"
                rowobj.Amount = data.Debit
            End If
        Next
        If _view.mode = "add" And data.JournalEntryMainID = 0 Then
            If model.InsertEntries(data, _view.JournalObj) Then
                MsgBox("Successfully Save", MsgBoxStyle.Information, "Journal Entry")
                editJournal(data.JournalEntryMainID)
                _view.JlistPresenter.refresh()
            End If
        Else
            If model.UpdateEntries(data, _view.JournalObj) Then
                MsgBox("Successfully Updated", MsgBoxStyle.Information, "Journal Entry")
                editJournal(data.JournalEntryMainID)
                _view.JlistPresenter.refresh()
            End If
        End If

    End Sub

End Class
