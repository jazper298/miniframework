﻿Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow
Imports AForge.Imaging
Imports System.IO
Imports System.Drawing

Public Class PersonSetupRecordPresenter

    Private _view As iPersonSetupRecord
    Private data As New PersonSetup
    Private model As New PersonSetupModel

    Public Sub New(ByVal view As iPersonSetupRecord, ByVal Optional refresh As Boolean = False)
        _view = view
    End Sub

    Public Sub edit(ByVal ID As Integer)
        Dim dt As DataTable

        dt = model.GetRecord(ID)

        Try

            data.PersonID = ID
            _view.title = dt(0)("Name").ToString

            _view.Name = dt(0)("Name").ToString
            _view.Age = dt(0)("Age").ToString
            '_view.Status = dt(0)("Status")

            If _view.mode <> "view" Then
                _view.mode = "edit"
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Public Sub save()
        Cursor.Current = Cursors.WaitCursor

        data.StatusID = 2
        data.Name = _view.Name
        data.Age = _view.Age
        data.Picture = _view.YourPicture

        If data.PersonID = 0 Then
            If model.Insert(data) Then
                saveCam
                MsgBox("Successfully Save", MsgBoxStyle.Information, "Person Setup")
                edit(data.PersonID)
                _view.ListPresenter.refresh()
            End If
        Else
            If model.Update(data) Then
                saveCam()
                MsgBox("Successfully Updated", MsgBoxStyle.Information, "Person Setup")
                edit(data.PersonID)
                _view.ListPresenter.refresh()
            End If
        End If

    End Sub


    Public Sub saveDraft()
        Cursor.Current = Cursors.WaitCursor

        data.StatusID = 1

        data.Name = _view.Name
        data.Age = _view.Age

        If data.PersonID = 0 Then
            If model.Insert(data) Then
                MsgBox("Successfully Save", MsgBoxStyle.Information, "Person Setup")
                edit(data.PersonID)
                _view.ListPresenter.refresh()
            End If
        Else
            If model.Update(data) Then
                MsgBox("Successfully Updated", MsgBoxStyle.Information, "Person Setup")
                edit(data.PersonID)
                _view.ListPresenter.refresh()
            End If
        End If


    End Sub
    Public Sub capture()
        If start = 1 Then
            data.Picture = _view.YourPicture
            data.Camera = _view.Camera
            _view.YourPicture = _view.Camera
        Else
            MsgBox("please start camera to take picture")
        End If

    End Sub

    Public Sub saveCam()

        Dim fileName As String = data.PersonID

        If _view.YourPicture Is Nothing Then
            MsgBox("please take your picture")
        Else
            _view.Dialog.DefaultExt = ".jpg"
            _view.YourPicture.Save("C:\Users\ELF-Laptop\Desktop\JAZ\MiniFramework\Images\" & fileName & ".jpg", Imaging.ImageFormat.Jpeg)
        End If

        _view.YourPicture = Nothing

    End Sub

    Dim Camera1s As VideoCaptureDevice
    Dim bmp As Bitmap
    Dim cameras As VideoCaptureDeviceForm
    Dim start As Integer

    Public Sub startCam()

        data.WebCam = _view.WebCam
        Dim cameras As VideoCaptureDeviceForm = New VideoCaptureDeviceForm
        If cameras.ShowDialog() = DialogResult.OK Then
            _view.WebCam = cameras.VideoDevice
            AddHandler _view.WebCam.NewFrame, New NewFrameEventHandler(AddressOf Captured)
            _view.Camera = data.Camera
            _view.WebCam.Start()
            start = 1
        End If
    End Sub

    Public Sub Captured(obj As Object, e As NewFrameEventArgs)
        bmp = DirectCast(e.Frame.Clone(), Bitmap)
        data.Camera = DirectCast(e.Frame.Clone(), Bitmap)
        _view.Camera = data.Camera
    End Sub

    Public Sub closeCam()
        If start = 1 Then
            _view.WebCam.Stop()
        End If
    End Sub

End Class
