﻿Imports AForge.Video.DirectShow

Public Interface iPersonSetupCamera
    Property title As String
    Property selectedID As Integer
    Property Camera As Bitmap
    Property YourPicture As Bitmap
    Property Dialog As SaveFileDialog

    Property Camera1 As Object
    Property YourPicture1 As Object

    Property ListPresenter As PersonSetupCameraPresenter
    Property WebCam As VideoCaptureDevice
End Interface
