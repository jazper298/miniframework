﻿Imports AForge.Video.DirectShow
Public Interface iPersonSetupRecord

    Property title As String

    'Property Status As String
    Property ListPresenter As PersonSetupListPresenter
    Property mode As String

    Property Name As String
    Property Age As String
    Property Camera As Bitmap
    Property YourPicture As Bitmap
    Property Dialog As SaveFileDialog
    Property WebCam As VideoCaptureDevice
End Interface
