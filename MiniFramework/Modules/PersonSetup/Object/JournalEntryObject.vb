﻿Public Class JournalEntryObject


    Property AccountEntryDetailID As Integer
    Property JournalEntryDetailID As Integer
    Property JournalEntryMainID As Integer
    Property AccountTitleID As Integer
    Property ResponsibleCenter As Integer
    Property TransactionDate As Date

    Property AccountName As String
    Property AccountCode As Integer
    Property Pr As String
    Property Amount As Decimal
    Property Debit As Decimal
    Property Credit As Decimal
    Property Type As String


End Class
