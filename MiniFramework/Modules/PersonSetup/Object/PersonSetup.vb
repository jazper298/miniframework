﻿Imports AForge.Video.DirectShow

Public Class PersonSetup

    Property PersonID As Integer
    Property Name As String
    Property Age As String
    Property Image As String
    Property StartTime As String
    Property EndTime As String
    Property StatusID As Integer

    Property Camera As Bitmap
    Property Picture As Bitmap
    Property Dialog As SaveFileDialog
    Property WebCam As VideoCaptureDevice

    Property Camera1 As Object
    Property Picture1 As Object
End Class
