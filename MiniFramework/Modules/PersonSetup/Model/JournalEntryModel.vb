﻿Public Class JournalEntryModel


    Public Function GetQueryReports() As Object
        Dim ObjectList As String = "
            SELECT 
        ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"",
        ""ACCOUNTING"".""JournalEntryDetail"".""AccountTitleID"",
        ""ACCOUNTING"".""JournalEntryDetail"".""Amount"",
        ""ACCOUNTING"".""JournalEntryDetail"".""Type"",
        ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryDetailID"",
        ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"",
        ""ACCOUNTING"".""AccountTitle"".""AccountCode"",
        ""ACCOUNTING"".""AccountTitle"".""AccountName"",
        ""ACCOUNTING"".""JournalEntryMain"".""ResponsibleCenter""
        FROM
        ""ACCOUNTING"".""JournalEntryDetail""
        INNER Join ""ACCOUNTING"".""AccountTitle"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"" = ""ACCOUNTING"".""JournalEntryDetail"".""AccountTitleID""
        INNER Join ""ACCOUNTING"".""JournalEntryMain"" ON ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" = ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID""
        WHERE
        ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" = 1
        ORDER BY
        ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryDetailID"" ASC

        "
        Return DataSource(ObjectList)
    End Function

    Public Function GetResponsible() As Object
        Dim ObjectList As String = "
            SELECT 
           *
            FROM 
            ""ACCOUNTING"".""ResponsibleCenter"" 

        "
        Return DataSource(ObjectList)
    End Function

    Public Function GetAccountName() As Object
        Dim ObjectList As String = "
            SELECT 
            ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"",
            ""ACCOUNTING"".""AccountTitle"".""AccountName"",
            ""ACCOUNTING"".""AccountTitle"".""AccountCode""
            FROM 
            ""ACCOUNTING"".""AccountTitle"" 
            WHERE 
            ""ACCOUNTING"".""AccountTitle"".""AccountTypeID"" = 4 
            ORDER BY
            ""ACCOUNTING"".""AccountTitle"".""AccountName"" ASC
        "
        Return DataSource(ObjectList)
    End Function

    Public Function GetAccountCode(ID As Integer) As Object
        Dim ObjectList As String = "
            SELECT 
            ""ACCOUNTING"".""AccountTitle"".""AccountCode""
            FROM 
            ""ACCOUNTING"".""AccountTitle""
            WHERE 
            ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"" = '" & ID & "' 
        "
        Return DataSource(ObjectList)
    End Function
    Dim objs As iJournalEntryVoucher
    Public Function InsertEntries(ByRef objs As Object, ByRef obj As List(Of JournalEntryObject)) As Object

        Dim sql As String = ""
        Dim dt As DataTable
        StartTransaction()
        Try
            sql = "
                INSERT into ""ACCOUNTING"".""JournalEntryMain"" (""TransactionDate"")
                VALUES ('" & objs.TransactionDate & "') RETURNING ""JournalEntryMainID""
            "
            dt = DataSource(sql, True)

            For Each rowobj In obj
                sql = "
                INSERT into ""ACCOUNTING"".""JournalEntryDetail"" (""JournalEntryMainID"",""AccountTitleID"",""Amount"",""Type"")
                VALUES ('" & dt(0)("JournalEntryMainID") & "','" & rowobj.AccountName & "', '" & rowobj.Amount & "', '" & rowobj.Type & "')
                "
                DataSource(sql, True)
            Next
            commitQuery()
        Catch ex As Exception
            MsgBox("test " + ex.Message)
            rollbackQuery()
            EndTransaction()
        End Try
        EndTransaction()
        objs.JournalEntryMainID = dt(0)("JournalEntryMainID")
        Return True
    End Function

    Public Function UpdateEntries(ByRef objs As Object, ByRef obj As List(Of JournalEntryObject)) As Object
        Dim sql As String = ""
        Dim dt As DataTable
        StartTransaction()

        Try
            sql = "UPDATE  ""ACCOUNTING"".""JournalEntryMain"" SET ""TransactionDate"" = '" & objs.TransactionDate & "'WHERE ""JournalEntryMainID"" = '" & objs.JournalEntryMainID & "' AND ""AccountTitleID"" = '" & objs.AccountTitleID & "' "
            DataSource(sql, True)

            sql = "SELECT * FROM ""ACCOUNTING"".""JournalEntryDetail"" WHERE ""JournalEntryMainID"" = '" & objs.JournalEntryMainID & "' "
            MsgBox(objs.JournalEntryMainID)
            Dim ss = DataSource(sql, True)
            'MsgBox(ss.Rows.Count)
            If ss.Rows.Count >= 1 Then
                For Each rowobj In obj
                    sql = "UPDATE ""ACCOUNTING"".""JournalEntryDetail"" SET ""AccountTitleID"" = '" & rowobj.AccountName & "',""Amount"" = '" & rowobj.Amount & "',""Type"" = '" & rowobj.Type & "' 
                    WHERE  ""JournalEntryMainID"" = '" & objs.JournalEntryMainID & "' "
                    DataSource(sql, True)
                Next
            Else
                For Each rowobj In obj
                    sql = "UPDATE ""ACCOUNTING"".""JournalEntryDetail"" SET ""AccountTitleID"" = '" & rowobj.AccountName & "',""Amount"" = '" & rowobj.Amount & "', ""Type"" = '" & rowobj.Type & "' 
                    WHERE ""JournalEntryMainID"" = '" & objs.JournalEntryMainID & "'"
                    DataSource(sql, True)
                Next
            End If

            commitQuery()
        Catch ex As Exception

            MsgBox(ex.Message)
            rollbackQuery()
            EndTransaction()

        End Try
        EndTransaction()

        Return True
    End Function

    Public Function GetJournalRecord(ID As Integer) As Object
        Dim ObjectList As String = "             
                SELECT
                ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" AS ""_JournalEntryMainID"",
                ""ACCOUNTING"".""JournalEntryMain"".""TransactionDate"",
                ""ACCOUNTING"".""JournalEntryMain"".""ResponsibleCenter"",
                ""ACCOUNTING"".""AccountTitle"".""AccountName"",
                ""ACCOUNTING"".""AccountTitle"".""AccountCode"",
                ""ACCOUNTING"".""JournalEntryDetail"".""Amount"",
                ""ACCOUNTING"".""JournalEntryDetail"".""Type"",
                ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryDetailID"" AS ""_JournalEntryDetailID""
                FROM
                ""ACCOUNTING"".""JournalEntryMain""
                INNER Join ""ACCOUNTING"".""JournalEntryDetail"" ON ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" = ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID""
                INNER Join ""ACCOUNTING"".""AccountTitle"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"" = ""ACCOUNTING"".""JournalEntryDetail"".""AccountTitleID""
                INNER Join ""ACCOUNTING"".""AccountType"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTypeID"" = ""ACCOUNTING"".""AccountType"".""AccountTypeID""
                WHERE
                ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" = '" & ID & "'
                "
        Return DataSource(ObjectList)
#Region ""
        'Select Case
        '""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" as ""_JournalEntryMainID"",
        '""ACCOUNTING"".""JournalEntryMain"".""TransactionDate"",
        '""ACCOUNTING"".""JournalEntryMain"".""ResponsibleCenter"",
        '""ACCOUNTING"".""AccountTitle"".""AccountName"",
        '""ACCOUNTING"".""AccountTitle"".""AccountCode"",
        '""ACCOUNTING"".""JournalEntryDetail"".""Amount"",
        '""ACCOUNTING"".""JournalEntryDetail"".""Type""
        'FROM
        '""ACCOUNTING"".""JournalEntryMain""
        'INNER Join ""ACCOUNTING"".""JournalEntryDetail"" ON ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" = ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID""
        'INNER Join ""ACCOUNTING"".""AccountTitle"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"" = ""ACCOUNTING"".""JournalEntryDetail"".""AccountTitleID""
        'INNER Join ""ACCOUNTING"".""AccountType"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTypeID"" = ""ACCOUNTING"".""AccountType"".""AccountTypeID"" 
        'WHEREss
        '""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" = '" & ID & "' 
#End Region


    End Function

    Public Function GetAllJournalRecord2() As Object

        Dim ObjectList As String = ""
        ObjectList = "

       SELECT
            ""_JournalEntryMainID"",
            ""TransactionDate"",
            ""ResponsibleCenter"",      
            ""AccountName"",
            ""AccountCode"",

            CASE WHEN ""Type"" = 'Debit' THEN ""Amount"" 
                ELSE 0 END as ""Debit"",
            CASE WHEN ""Type"" = 'Credit' THEN ""Amount"" 
                ELSE 0 END as ""Credit""
               
            FROM(
            SELECT
            ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" AS ""_JournalEntryMainID"",
            ""ACCOUNTING"".""JournalEntryMain"".""TransactionDate"",
            ""ACCOUNTING"".""JournalEntryMain"".""ResponsibleCenter"",
            ""ACCOUNTING"".""AccountTitle"".""AccountName"",
            ""ACCOUNTING"".""AccountTitle"".""AccountCode"",
            ""ACCOUNTING"".""JournalEntryDetail"".""Amount"",
            ""ACCOUNTING"".""JournalEntryDetail"".""Type"",
            ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryDetailID"" AS ""_JournalEntryDetailID""
            FROM
            ""ACCOUNTING"".""JournalEntryMain""
            INNER Join ""ACCOUNTING"".""JournalEntryDetail"" ON ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" = ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID""
            INNER Join ""ACCOUNTING"".""AccountTitle"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTitleID"" = ""ACCOUNTING"".""JournalEntryDetail"".""AccountTitleID""
            INNER Join ""ACCOUNTING"".""AccountType"" ON ""ACCOUNTING"".""AccountTitle"".""AccountTypeID"" = ""ACCOUNTING"".""AccountType"".""AccountTypeID""

            ORDER BY ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" ASC
            )a
            
            "

        Return DataSource(ObjectList)
    End Function

    Public Function GetAllJournalRecord() As Object
        Dim ObjectList As String = "

            SELECT
            ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryDetailID"" as ""_JournalEntryDetailID"",
            ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" as ""_JournalEntryMainID"",
            ""ACCOUNTING"".""JournalEntryMain"".""ResponsibleCenter"",
            ""ACCOUNTING"".""AccountTitle"".""AccountName"",
            ""ACCOUNTING"".""JournalEntryDetail"".""Amount"",
            ""ACCOUNTING"".""JournalEntryDetail"".""Type""

            FROM
            ""ACCOUNTING"".""JournalEntryDetail""
            INNER Join ""ACCOUNTING"".""AccountTitle"" ON ""ACCOUNTING"".""JournalEntryDetail"".""AccountTitleID"" = ""ACCOUNTING"".""AccountTitle"".""AccountTitleID""
            INNER Join ""ACCOUNTING"".""JournalEntryMain"" ON ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryMainID"" = ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID""
            ORDER BY
            ""ACCOUNTING"".""JournalEntryDetail"".""JournalEntryDetailID"" ASC
               "


        Return DataSource(ObjectList)

    End Function

    Public Function DeleteEntries(ID As Integer) As Object
        Dim sql As String
        sql = "DELETE FROM ""ACCOUNTING"".""JournalEntryMain"" WHERE ""ACCOUNTING"".""JournalEntryMain"".""JournalEntryMainID"" = '" & ID & "' "
        DataSource(sql)
        Return True
    End Function


End Class
