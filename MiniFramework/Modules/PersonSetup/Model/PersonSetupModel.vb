﻿Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow
Imports AForge.Imaging
Imports System.IO
Public Class PersonSetupModel

    Public Function Insert(ByRef obj As Object) As Object
        Dim sql As String = ""
        Dim dt As DataTable

        StartTransaction()

        Try
            sql = "
                INSERT into ""Test"".""Person"" (""Name"",""Age"")
                VALUES ('" & obj.Name & "','" & obj.Age & "') RETURNING ""PersonID""
            "
            dt = DataSource(sql, True)

            sql = "
                INSERT INTO ""Test"".""PersonStatus"" (""PersonID"",""StartTime"",""PersonStatusTypeID"") VALUES ('" & dt(0)("PersonID") & "',CURRENT_TIMESTAMP,'" & 2 & "') 
            "
            DataSource(sql, True)

            commitQuery()

        Catch ex As Exception

            MsgBox("test " + ex.Message)
            rollbackQuery()
            EndTransaction()

        End Try
        EndTransaction()

        obj.PersonID = dt(0)("PersonID")

        Return True
    End Function

    Public Function Delete(ID As Integer) As Object
        Dim sql As String

        sql = "DELETE FROM ""Test"".""Person"" WHERE ""PersonID"" = '" & ID & "'"
        DataSource(sql)
        Return True
    End Function

    Public Function Update(obj As Object) As Object
        Dim sql As String = ""
        Dim dt As DataTable
        StartTransaction()

        Try

            sql = "
                UPDATE ""Test"".""Person"" SET ""Name"" = '" & obj.Name & "',""Age"" = '" & obj.Age & "'
                WHERE ""PersonID"" = '" & obj.PersonID & "'                 
                "
            DataSource(sql, True)

            sql = "SELECT * FROM ""Test"".""PersonStatus"" WHERE  ""PersonID"" = '" & obj.PersonID & "' AND ""EndTime""  IS NULL  AND ""PersonStatusTypeID"" = '" & obj.statusID & "' "
            Dim ss = DataSource(sql, True)

            If ss.Rows.Count = 0 Then
                sql = "                
                    UPDATE ""Test"".""PersonStatus"" SET ""EndTime"" = CURRENT_TIMESTAMP WHERE  ""PersonID"" = '" & obj.PersonID & "'               
                "
                DataSource(sql, True)

                sql = "                
                    INSERT INTO ""Test"".""PersonStatus"" (""PersonID"",""StartTime"",""PersonStatusTypeID"") VALUES ('" & obj.PersonID & "',CURRENT_TIMESTAMP,'" & obj.statusID & "')                 
                "
                DataSource(sql, True)
            End If

            commitQuery()

        Catch ex As Exception

            MsgBox(ex.Message)
            rollbackQuery()
            EndTransaction()

        End Try
        EndTransaction()

        Return True
    End Function


    Public Function GetRecord(ID As Integer) As Object
        Dim ObjectList As String = "
              
               SELECT ""Test"".""Person"".""Name"", ""Test"".""Person"".""Age"", ""Test"".""PersonStatusType"".""Names"", ""Test"".""PersonStatus"".""PersonStatusID"" 
                FROM 
               ""Test"".""PersonStatusType"" INNER JOIN (""Test"".""Person"" INNER JOIN ""Test"".""PersonStatus"" ON ""Test"".""Person"".""PersonID"" = ""Test"".""PersonStatus"".""PersonID"") 
               ON ""Test"".""PersonStatusType"".""PersonStatusTypeID"" = ""Test"".""PersonStatus"".""PersonStatusTypeID"" WHERE ""Test"".""Person"".""PersonID""  = '" & ID & "'                 
                "

        Return DataSource(ObjectList)
    End Function

    Public Function GetAllRecord() As Object
        Dim ObjectList As String = "
                SELECT ""Test"".""Person"".""PersonID"" as ""_PersonID"",
                ""Test"".""Person"".""Name"",
                ""Test"".""Person"".""Age"",
                ""Test"".""PersonStatusType"".""Names"" AS ""Status""
                FROM ""Test"".""PersonStatusType"" INNER JOIN (""Test"".""Person"" INNER JOIN ""Test"".""PersonStatus"" ON ""Test"".""Person"".""PersonID"" = ""Test"".""PersonStatus"".""PersonID"") 
                ON ""Test"".""PersonStatusType"".""PersonStatusTypeID"" = ""Test"".""PersonStatus"".""PersonStatusTypeID"" WHERE ""Test"".""PersonStatus"".""EndTime"" is NULL
                "
        Return DataSource(ObjectList)
    End Function

    Public Function Deactivate(ID As Integer) As Object

        Dim sql As String = ""
        Dim dt As DataTable

        sql = "SELECT * FROM ""Test"".""PersonStatus"" WHERE ""PersonID"" = '" & ID & "' AND ""EndTime"" IS NULL AND ""PersonStatusTypeID"" = '1' "

        'sql = "SELECT * FROM ""Treasury"".""BankSetupStatus"" WHERE ""BankSetupID"" = '" & ID & "' AND ""EndTime"" IS NULL AND ""BankSetupStatusTypeID"" = '1' "
        dt = DataSource(sql)


        If dt.Rows.Count > 0 Then
            MsgBox("Draft status cannot be deactivated", MsgBoxStyle.Information, "Deactivation")
            Return False
        End If

        StartTransaction()
        Try

            DataSource("UPDATE ""Test"".""PersonStatus"" SET ""EndTime""= current_timestamp WHERE ""PersonID""='" & ID & "' AND ""EndTime"" IS NULL RETURNING ""PersonStatusTypeID"" ", True)
            DataSource("INSERT INTO ""Test"".""PersonStatus"" (""PersonID"",""StartTime"",""PersonStatusTypeID"") VALUES ('" & ID & "', CURRENT_TIMESTAMP ,'3') ", True)

            commitQuery()
        Catch ex As Exception
            rollbackQuery()
            MsgBox("text" + ex.Message)
            Return False
        End Try
        EndTransaction()
        Return True
    End Function

    Public Function Reactivate(ID As Integer) As Object
        Dim sql As String = ""
        Dim dt As DataTable

        sql = "SELECT * FROM ""Test"".""PersonStatus"" WHERE ""PersonID"" = '" & ID & "' AND ""EndTime"" IS NULL AND ""PersonStatusTypeID"" = '1' "
        dt = DataSource(sql)


        If dt.Rows.Count > 0 Then
            MsgBox("Draft status cannot be deactivated", MsgBoxStyle.Information, "Deactivation")
            Return False
        End If

        StartTransaction()
        Try

            DataSource("UPDATE ""Test"".""PersonStatus"" SET ""EndTime""= current_timestamp WHERE ""PersonID""='" & ID & "' AND ""EndTime"" IS NULL RETURNING ""PersonStatusTypeID"" ", True)
            DataSource("INSERT INTO ""Test"".""PersonStatus"" (""PersonID"",""StartTime"",""PersonStatusTypeID"") VALUES ('" & ID & "',CURRENT_TIMESTAMP,'2') ", True)

            commitQuery()
        Catch ex As Exception
            rollbackQuery()
            MsgBox(ex.Message)
            Return False
        End Try
        EndTransaction()
        Return True
    End Function



End Class
