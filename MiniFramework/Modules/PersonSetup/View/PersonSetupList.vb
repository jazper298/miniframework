﻿Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports ERP

Public Class PersonSetupList
    Implements iPersonSetupList
    Public listPresent As PersonSetupListPresenter
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        listPresent = New PersonSetupListPresenter(Me)

    End Sub

    Dim Data As DataTable
    Public Property dt As DataTable Implements iPersonSetupList.dt
        Get
            Return Data
        End Get
        Set(value As DataTable)
            Data = value

            Dim list As New List(Of String)
            For Each dc As DataColumn In Data.Columns
                If dc.ColumnName.Substring(0, 1) = "_" Then
                    dc.ColumnName = Replace(dc.ColumnName, "_", "")
                    list.Add(dc.ColumnName)
                End If
            Next

            Dim i As Integer = GridView1.FocusedRowHandle
            Dim row As DataRow = GridView1.GetDataRow(i)
            Dim selected As Int64

            If row IsNot Nothing Then
                selected = row("PersonID")
            End If

            GridView1.BeginDataUpdate()

            If Data.Rows.Count > GridView1.RowCount Then
                selected = Data(Data.Rows.Count - 1)("PersonID")
            End If

            GridControl1.DataSource = Data

            Try
                For Each column As Columns.GridColumn In GridView1.Columns
                    For Each li In list
                        If column.FieldName = li Then
                            column.Visible = False
                        End If
                    Next
                Next
                GridView1.EndDataUpdate()
                Dim rowHandle = GridView1.LocateByValue("PersonID", selected)
                GridView1.ClearSelection()
                GridView1.FocusedRowHandle = rowHandle
                GridView1.SelectRow(rowHandle)

            Catch ex As Exception

            End Try
        End Set
    End Property
    Dim _selectedID As Integer
    Public Property selectedID As Integer Implements iPersonSetupList.selectedID
        Get
            Return _selectedID
        End Get
        Set(value As Integer)
            _selectedID = value
        End Set
    End Property

    Public Property mode As String Implements iPersonSetupList.mode
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As String)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Property gView As GridView Implements iPersonSetupList.gView
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As GridView)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Property gControl As GridControl Implements iPersonSetupList.gControl
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As GridControl)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Property ListPresenter As PersonSetupListPresenter Implements iPersonSetupList.ListPresenter
        Get
            Return listPresent
        End Get
        Set(value As PersonSetupListPresenter)
            listPresent = value
        End Set
    End Property

    Private Sub PersonSetupList_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class