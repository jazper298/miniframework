﻿Imports Mini
Imports EEGS
Imports ERP
Imports AForge.Video.DirectShow

Public Class PersonSetupRecord
    Implements iPersonSetupRecord

    Private presenter As PersonSetupRecordPresenter
    Private _listpresenter As PersonSetupListPresenter
    Private Camera1 As VideoCaptureDevice
    Public Sub New(ByVal listpresenter As PersonSetupListPresenter)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _listpresenter = listpresenter
        presenter = New PersonSetupRecordPresenter(Me)

        If _listpresenter.SelectedID > 0 Then
            presenter.edit(_listpresenter.SelectedID)
        End If

    End Sub

    Public Property title As String Implements iPersonSetupRecord.title
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As String)
            Me.Text = "PersonSetup - " & value
        End Set
    End Property

    'Public Property Status As String Implements iPersonSetupRecord.Status
    '    Get
    '        Return txtStatus.Text
    '    End Get
    '    Set(value As String)
    '        txtStatus.Text = value
    '    End Set
    'End Property

    Public Property ListPresenter As PersonSetupListPresenter Implements iPersonSetupRecord.ListPresenter
        Get
            Return _listpresenter
        End Get
        Set(value As PersonSetupListPresenter)
            _listpresenter = value
        End Set
    End Property
    Dim _mode As String
    Public Property mode As String Implements iPersonSetupRecord.mode
        Get
            Return _mode
        End Get
        Set(value As String)
            _mode = value

            If _mode = "edit" Then
                btnSave.Text = "Update"
                FormProperty(Me, True)
                btnSave.Enabled = True
                chkEdit.Checked = True
            ElseIf _mode = "view" Then
                btnSave.Enabled = False
                FormProperty(Me)
            End If
            'sstxtStatus.ReadOnly = True
        End Set
    End Property

    Public Property Age As String Implements iPersonSetupRecord.Age
        Get
            Return txtAge.Text
        End Get
        Set(value As String)
            txtAge.Text = value
        End Set
    End Property

    Private Property iPersonSetupRecord_Name As String Implements iPersonSetupRecord.Name
        Get
            Return txtName.Text
        End Get
        Set(value As String)
            txtName.Text = value
        End Set
    End Property

    Public Property Camera As Bitmap Implements iPersonSetupRecord.Camera
        Get
            Return PictureBox1.Image
        End Get
        Set(value As Bitmap)
            PictureBox1.Image = value
        End Set
    End Property

    Public Property YourPicture As Bitmap Implements iPersonSetupRecord.YourPicture
        Get
            Return PictureBox2.Image
        End Get
        Set(value As Bitmap)
            PictureBox2.Image = value
        End Set
    End Property

    Public Property Dialog As SaveFileDialog Implements iPersonSetupRecord.Dialog
        Get
            Return SaveFileDialog1
        End Get
        Set(value As SaveFileDialog)
            SaveFileDialog1 = value
        End Set
    End Property

    Public Property WebCam As VideoCaptureDevice Implements iPersonSetupRecord.WebCam
        Get
            Return Camera1
        End Get
        Set(value As VideoCaptureDevice)
            Camera1 = value
        End Set
    End Property

    Private Sub PersonSetup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'btnStop.Enabled = False
        'btnCapture.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Form_validation(Me) Then
            presenter.save()
            Me.Close()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub chkEdit_CheckedChanged(sender As Object, e As EventArgs) Handles chkEdit.CheckedChanged
        If chkEdit.Checked = True Then
            mode = "edit"
        Else
            mode = "view"
        End If
    End Sub

    Private Sub btnSaveDraft_Click(sender As Object, e As EventArgs) Handles btnSaveDraft.Click
        If Form_validation(Me) Then
            presenter.saveDraft()
        End If
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If Form_validation(Me) Then
            presenter.startCam()
            'btnStop.Enabled = True
            'btnCapture.Enabled = True
            'btnStart.Enabled = False
        End If
    End Sub

    Private Sub btnCapture_Click(sender As Object, e As EventArgs) Handles btnCapture.Click
        If Form_validation(Me) Then
            presenter.capture()
            'btnStart.Enabled = True
        End If
    End Sub

    Private Sub PersonSetupRecord_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Form_validation(Me) Then
            presenter.closeCam()

        End If
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        If Form_validation(Me) Then
            presenter.closeCam()
            'btnStart.Enabled = True
        End If
    End Sub
End Class