﻿Imports System.Text.RegularExpressions
Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI
Imports Mini

Public Class ReportRecord

    Implements iJournalEntryVoucher

    Dim report As New XtraJournalEntryVoucher
    Dim subreport As New XtraReportSubEntryVoucher

    Dim SubReports As New XRSubreport

    Dim obj As New JournalEntryVoucherObject

    Private model As New JournalEntryModel
    Private data As New JournalEntryObject
    Dim dt As DataTable

    Private presenter As JournalEntryVoucherPresenter
    Private _listpresenter As JournalEntryVoucherListPresenter

    Dim type As String
    Dim debits As String
    Dim credits As String
    Dim totalCredit As Double
    Dim totalDebit As Double
    Dim today As String

    Public Sub New(ByVal listpresenter As JournalEntryVoucherListPresenter)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _listpresenter = listpresenter
        presenter = New JournalEntryVoucherPresenter(Me)

        If _listpresenter.SelectedID > 0 Then
            '     presenter.editJournal(_listpresenter.SelectedID)
            Dim dt As New DataTable
            dt = presenter.getJournal(_listpresenter.SelectedID)
            '   GridControl1.DataSource = dt
            GridControl2.DataSource = dt
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ShowResponsible()
        ShowSection1()
        'ShowSection2()
    End Sub

    Private Sub ShowSection1()
        Dim today As String = DateTimePicker1.Value.ToString("MMMM dd, yyyy")
        With obj
            .JournalDate = today.ToUpper
            .Debit = New List(Of JournalEntryVoucherObject.Entries)
            .Credit = New List(Of JournalEntryVoucherObject.Entries)
            .Section = New List(Of JournalEntryVoucherObject)
        End With

        dt = model.GetQueryReports()

        For i As Integer = 0 To 0

            If dt.Rows.Count > 0 Then

                For Each row As DataRow In dt.Rows
                    Dim rowobj As New JournalEntryVoucherObject.Entries
                    rowobj.ResponsibleCenter = row("ResponsibleCenter").ToString
                    rowobj.Type = row("Type").ToString

                    type = rowobj.Type
                    'If 'row("Type").ToString
                    If type.Equals("Debit") Then

                        rowobj.AccountTitle = row("AccountName").ToString
                        rowobj.Code = row("AccountCode").ToString
                        rowobj.Debit = Format(row("Amount"), "#,###,##0.00")
                        rowobj.TotalDebit = dt.Compute("Sum(Amount)", String.Empty)
                        obj.Debit.Add(rowobj)
                    Else
                        rowobj.AccountTitle = row("AccountName").ToString
                        rowobj.Code = row("AccountCode").ToString
                        rowobj.Credit = Format(row("Amount"), "#,###,##0.00")
                        rowobj.TotalCredit = dt.Compute("Sum(Amount)", String.Empty)
                        obj.Credit.Add(rowobj)
                    End If
                Next
            End If
            obj.Section.Add(obj)
        Next
        report.BindingSource1.DataSource = obj
        Dim subr As New XtraReportSubEntryVoucher
        subr.BindingSource1.DataSource = obj
        report.XrSubreport1.ReportSource = subr

        report.ShowPreview
    End Sub

    Private Sub ShowSection2()
        Dim today As String = DateTimePicker1.Value.ToString("MMMM dd, yyyy")
        With obj
            .JournalDate = today.ToUpper
            .Debit = New List(Of JournalEntryVoucherObject.Entries)
            .Credit = New List(Of JournalEntryVoucherObject.Entries)
            .Section = New List(Of JournalEntryVoucherObject)
        End With

        dt = model.GetQueryReports()

        For i As Integer = 0 To 0

            If dt.Rows.Count > 0 Then

                For Each row As DataRow In dt.Rows
                    Dim rowobj As New JournalEntryVoucherObject.Entries

                    rowobj.Type = row("Type").ToString
                    type = rowobj.Type
                    'If 'row("Type").ToString
                    If type.Equals("Debit") Then
                        rowobj.AccountTitle = row("AccountName").ToString
                        rowobj.Code = row("AccountCode").ToString
                        rowobj.Debit = Format(row("Amount"), "#,###,##0.00")
                        obj.Debit.Add(rowobj)
                    Else
                        rowobj.AccountTitle = row("AccountName").ToString
                        rowobj.Code = row("AccountCode").ToString
                        rowobj.Credit = Format(row("Amount"), "#,###,##0.00")
                        obj.Credit.Add(rowobj)
                    End If
                Next
            End If
            obj.Section.Add(obj)
        Next
        report.BindingSource1.DataSource = obj
        Dim subr As New XtraReportSub2EntryVoucher
        subr.BindingSource1.DataSource = obj
        report.XrSubreport2.ReportSource = subr

        report.ShowPreview
    End Sub

    Private Sub CalculateDebit()
        Dim sumObject As Object
        sumObject = dt.Compute("Sum(Amount)", String.Empty)
    End Sub

    Private Sub ShowResponsible()
        Dim today As String = DateTimePicker1.Value.ToString("MMMM dd, yyyy")
        With obj
            .JournalDate = today.ToUpper
            .Debit = New List(Of JournalEntryVoucherObject.Entries)
            .Credit = New List(Of JournalEntryVoucherObject.Entries)
            .Section = New List(Of JournalEntryVoucherObject)
        End With

        dt = model.GetQueryReports()

        For i As Integer = 0 To 0

            If dt.Rows.Count > 0 Then

                For Each row As DataRow In dt.Rows
                    Dim responsible As String
                    Dim s As String = row("ResponsibleCenter").ToString

                    Dim arr As String() = SplitWords(s)

                    For Each s In arr
                        Dim rowobj As New JournalEntryVoucherObject.Entries
                        rowobj.ResponsibleCenter = s
                        obj.Debit.Add(rowobj)
                    Next
                Next
            End If
            obj.Section.Add(obj)
        Next
        report.BindingSource1.DataSource = obj
        Dim subr As New XtraReportSubEntryVoucher
        subr.BindingSource1.DataSource = obj
        report.XrSubreport1.ReportSource = subr

        report.ShowPreview
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        Return Regex.Split(s, "\W+")
    End Function

    Dim journalEntryList As New List(Of JournalEntryObject)

    Dim _mode As String
    Public Property mode As String Implements iJournalEntryVoucher.mode
        Get
            Return _mode
        End Get
        Set(value As String)
            _mode = value

            If _mode = "edit" Then
                Button4.Text = "Update"
                FormProperty(Me, True)
                Button4.Enabled = True
                chkEdit.Checked = True
            End If
        End Set

    End Property

    Public Property _date As Date Implements iJournalEntryVoucher._date
        Get
            Return DateTimePicker1.Value.ToString("MMMM dd, yyyy")
        End Get
        Set(value As Date)
            DateTimePicker1.Value = value
        End Set
    End Property

    Public Property JournalObj As List(Of JournalEntryObject) Implements iJournalEntryVoucher.JournalObj
        Get
            Return journalEntryList
        End Get
        Set(value As List(Of JournalEntryObject))
            journalEntryList = value
        End Set
    End Property

    Public Property JlistPresenter As JournalEntryVoucherListPresenter Implements iJournalEntryVoucher.JlistPresenter
        Get
            Return _listpresenter
        End Get
        Set(value As JournalEntryVoucherListPresenter)
            _listpresenter = value
        End Set
    End Property


    Private Sub ReportRecord_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadResponsibleName()
        LoadAccountName()
        GridControl1.DataSource = journalEntryList

    End Sub

    Private Sub LoadResponsibleName()
        RepositoryItemGridLookUpEdit1.DataSource = model.GetResponsible()
        RepositoryItemGridLookUpEdit1.DisplayMember = "Name"
        RepositoryItemGridLookUpEdit1.ValueMember = "ResponsibleCenterID"

        GridControl1.DataSource = journalEntryList
    End Sub

    Private Sub LoadAccountName()
        RepositoryItemGridLookUpEdit2.DataSource = model.GetAccountName()
        RepositoryItemGridLookUpEdit2.DisplayMember = "AccountName"
        RepositoryItemGridLookUpEdit2.ValueMember = "AccountTitleID"

        GridControl1.DataSource = journalEntryList
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim row As New JournalEntryObject

        journalEntryList.Add(row)

        GridControl1.RefreshDataSource()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim row As New JournalEntryObject

        journalEntryList.Remove(row)

        GridControl1.RefreshDataSource()
    End Sub

    Private Sub RepositoryItemGridLookUpEdit2_EditValueChanged(sender As Object, e As EventArgs) Handles RepositoryItemGridLookUpEdit2.EditValueChanged
        Dim onclick As GridLookUpEdit = TryCast(sender, GridLookUpEdit)
        'XtraMessageBox.Show(Convert.ToString(onclick.EditValue))
        If Form_validation(Me) Then
            presenter.onClick(onclick.EditValue)
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Form_validation(Me) Then
            presenter.saveJournal()
            Me.Close()
        End If
    End Sub

    Private Sub chkEdit_CheckedChanged(sender As Object, e As EventArgs) Handles chkEdit.CheckedChanged

    End Sub
End Class