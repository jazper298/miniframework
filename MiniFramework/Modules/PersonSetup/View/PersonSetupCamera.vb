﻿Imports Mini
Imports ERP
Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow
Imports AForge.Imaging
Imports System.IO
Imports System.Drawing

Public Class PersonSetupCamera
    Implements iPersonSetupCamera

    Private presenter As PersonSetupCameraPresenter
    Private _listpresenter As PersonSetupCameraPresenter
    Private fileDialog As SaveFileDialog

    Public Sub New(ByVal listpresenter As PersonSetupCameraPresenter)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        _listpresenter = listpresenter
        presenter = New PersonSetupCameraPresenter(Me)
    End Sub

    Public Property ListPresenter As PersonSetupCameraPresenter Implements iPersonSetupCamera.ListPresenter
        Get
            Return _listpresenter
        End Get
        Set(value As PersonSetupCameraPresenter)
            _listpresenter = value
        End Set
    End Property

    Public Property YourPicture As Bitmap Implements iPersonSetupCamera.YourPicture
        Get
            Return PictureBox2.Image
        End Get
        Set(value As Bitmap)
            PictureBox2.Image = value
        End Set
    End Property

    Dim _selectedID As Integer
    Public Property selectedID As Integer Implements iPersonSetupCamera.selectedID
        Get
            Return _selectedID
        End Get
        Set(value As Integer)
            _selectedID = value
        End Set
    End Property

    Public Property title As String Implements iPersonSetupCamera.title
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As String)
            Me.Text = "PersonSetup - " & value
        End Set
    End Property

    Public Property Camera As Bitmap Implements iPersonSetupCamera.Camera
        Get
            Return PictureBox1.Image
        End Get
        Set(value As Bitmap)
            PictureBox1.Image = value
        End Set
    End Property

    Private Property iPersonSetupCamera_Camera1 As Object Implements iPersonSetupCamera.Camera1
        Get
            Return PictureBox1.Image
        End Get
        Set(value As Object)
            PictureBox1.Image = value
        End Set
    End Property

    Public Property YourPicture1 As Object Implements iPersonSetupCamera.YourPicture1
        Get
            Return PictureBox2.Image
        End Get
        Set(value As Object)
            PictureBox2.Image = value
        End Set
    End Property

    Public Property Dialog As SaveFileDialog Implements iPersonSetupCamera.Dialog
        Get
            Return SaveFileDialog1
        End Get
        Set(value As SaveFileDialog)
            SaveFileDialog1 = value
        End Set
    End Property

    Public Property WebCam As VideoCaptureDevice Implements iPersonSetupCamera.WebCam
        Get
            Return Camera1
        End Get
        Set(value As VideoCaptureDevice)
            Camera1 = value
        End Set
    End Property

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Form_validation(Me) Then
            presenter.save()
        End If
    End Sub

    Private Sub PersonSetupCamera_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        'If Form_validation(Me) Then
        '    presenter.startCam()
        'End If
    End Sub

    Private Sub btnCapture_Click(sender As Object, e As EventArgs) Handles btnCapture.Click
        If Form_validation(Me) Then
            presenter.capture()
        End If
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub

    Private Camera1 As VideoCaptureDevice
    Dim bmp As Bitmap

    Private Sub PersonSetupCamera_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Form_validation(Me) Then
            presenter.closeCam()
        End If
    End Sub

    Private Sub PersonSetupCamera_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        If Form_validation(Me) Then
            presenter.closeCam()
        End If
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If Form_validation(Me) Then
            presenter.startCam()
        End If
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        If Form_validation(Me) Then
            presenter.closeCam()
        End If
    End Sub
End Class