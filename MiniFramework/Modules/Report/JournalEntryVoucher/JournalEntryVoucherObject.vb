﻿Public Class JournalEntryVoucherObject

    Property JournalDate As String

    Property Debit As List(Of Entries)
    Property Credit As List(Of Entries)

    Property Section As List(Of JournalEntryVoucherObject)

    Class Entries
        Property ResponsibleCenter As String
        Property AccountTitle As String
        Property Type As String
        Property Code As String
        Property Pr As String
        Property Debit As String
        Property Credit As String
        Property TotalDebit As Double
        Property TotalCredit As Double
        Property Description1 As String
        Property Description2 As String
        Property PreparedBy As String
        Property ApprovedBy As String
    End Class


End Class
