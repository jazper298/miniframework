﻿Imports EEGS
Imports ERP

Public Class BankSetupRecord
    Implements iBankSetupRecord

    Private presenter As BankSetupRecordPresenter
    Private _listpresenter As BankSetupListPresenter
    Public Sub New(ByVal listpresenter As BankSetupListPresenter)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _listpresenter = listpresenter
        presenter = New BankSetupRecordPresenter(Me)

        If _listpresenter.SelectedID > 0 Then
            presenter.edit(_listpresenter.SelectedID)
        End If

    End Sub

    Public Property ListPresenter As BankSetupListPresenter Implements iBankSetupRecord.ListPresenter
        Get
            Return _listpresenter
        End Get
        Set(value As BankSetupListPresenter)
            _listpresenter = value
        End Set
    End Property
    Dim _mode As String
    Public Property mode As String Implements iBankSetupRecord.mode
        Get
            Return _mode
        End Get
        Set(value As String)
            _mode = value

            If _mode = "edit" Then
                btnSave.Text = "Update"
                FormProperty(Me, True)
                btnSave.Enabled = True
                chkEdit.Checked = True
            ElseIf _mode = "view" Then
                btnSave.Enabled = False
                FormProperty(Me)
            End If
            txtStatus.ReadOnly = True
        End Set
    End Property

    Public Property title As String Implements iBankSetupRecord.title
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As String)
            Me.Text = "BankSetup - " & value
        End Set
    End Property

    Public Property Status As String Implements iBankSetupRecord.Status
        Get
            Return txtStatus.Text
        End Get
        Set(value As String)
            txtStatus.Text = value
        End Set
    End Property

    Private Property iBankSetupRecord_Name As String Implements iBankSetupRecord.Name
        Get
            Return txtName.Text
        End Get
        Set(value As String)
            txtName.Text = value
        End Set
    End Property

    Public Property Description As String Implements iBankSetupRecord.Description
        Get
            Return TxtDescription.Text
        End Get
        Set(value As String)
            TxtDescription.Text = value
        End Set
    End Property

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Form_validation(Me) Then
            presenter.save()
        End If
    End Sub

    Private Sub btnSaveDraft_Click(sender As Object, e As EventArgs) Handles btnSaveDraft.Click
        If Form_validation(Me) Then
            presenter.saveDraft()
        End If
    End Sub

    Private Sub chkEdit_CheckedChanged(sender As Object, e As EventArgs) Handles chkEdit.CheckedChanged
        If chkEdit.Checked = True Then
            mode = "edit"
        Else
            mode = "view"
        End If
    End Sub
    Private Sub BankSetupRecord_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class