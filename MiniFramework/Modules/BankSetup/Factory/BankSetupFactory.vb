﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class BankSetupFactory


    Public Function ListForm() As Form
        Dim frm As New BankSetupList()
        Return frm
    End Function

    Public Function AddForm(ByVal List As Object)
        Cursor.Current = Cursors.WaitCursor
        Dim presenter As BankSetupListPresenter = CType(List, BankSetupListPresenter)
        presenter.SelectedID = 0
        Dim app As New BankSetupRecord(presenter)
        app.chkEdit.Enabled = True
        app.btnSaveDraft.Enabled = True
        app.ShowDialog()
        Cursor.Current = Cursors.Default
    End Function

    Public Sub EditForm(ByVal List As Object, ByVal dr As DataRowView)
        If dr IsNot Nothing Then
            Cursor.Current = Cursors.WaitCursor
            Dim presenter As BankSetupListPresenter = CType(List, BankSetupListPresenter)
            presenter.SelectedID = dr("BankSetupID").ToString
            Dim frm As New BankSetupRecord(presenter)
            frm.mode = "edit"
            frm.ShowDialog()
            Cursor.Current = Cursors.Default
        End If
    End Sub

    Public Function ViewForm(ByVal List As Object, ByVal dr As DataRowView)
        If dr IsNot Nothing Then
            Dim presenter As BankSetupListPresenter = CType(List, BankSetupListPresenter)
            presenter.SelectedID = dr("BankSetupID").ToString
            Dim app As New BankSetupRecord(presenter)
            app.mode = "view"
            app.chkEdit.Enabled = False
            app.btnSaveDraft.Enabled = False
            app.ShowDialog()
        End If

    End Function

    Public Sub Delete(ByVal List As Object, ByVal dr As DataRowView)
        Dim presenter As BankSetupListPresenter = CType(List, BankSetupListPresenter)
        Dim label As String
        If dr IsNot Nothing Then
            label = dr("Status").ToString()
            If label <> "Draft" Then
                MsgBox("Cannot Delete this Record, Data is in used", MsgBoxStyle.Exclamation, "Delete Record")
            Else
                presenter.SelectedID = dr("BankSetupID").ToString
                If MessageBox.Show("Are you sure you want to Delete", "Delete Record", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    If presenter.delete() Then
                        presenter.refresh()
                    End If
                End If
            End If
        End If

    End Sub

    Public Sub Deactivate(ByVal List As Object, ByVal dr As DataRowView)
        Cursor.Current = Cursors.WaitCursor
        If dr Is Nothing Then
            Exit Sub
        End If
        Dim presenter As BankSetupListPresenter = CType(List, BankSetupListPresenter)
        If MessageBox.Show("Are you sure you want to Deactivate this Record?", "Confirm Deactivation", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) = DialogResult.Yes Then
            presenter.SelectedID = dr("BankSetupID").ToString
            presenter.Deactivate()
            presenter.refresh()

        End If
        Cursor.Current = Cursors.Default

    End Sub

    Public Sub Reactivate(ByVal List As Object, ByVal dr As DataRowView)
        Cursor.Current = Cursors.WaitCursor
        If dr Is Nothing Then
            Exit Sub
        End If
        Dim presenter As BankSetupListPresenter = CType(List, BankSetupListPresenter)
        If MessageBox.Show("Are you sure you want to Reactivate this Record?", "Confirm Reactivation", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) = DialogResult.Yes Then
            presenter.SelectedID = dr("BankSetupID").ToString
            presenter.Reactivate()
            presenter.refresh()

        End If
        Cursor.Current = Cursors.Default

    End Sub

End Class
