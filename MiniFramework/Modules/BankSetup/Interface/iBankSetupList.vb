﻿Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid

Public Interface iBankSetupList
    Property dt As DataTable
    Property selectedID As Integer
    Property mode As String
    Property gView As GridView
    Property gControl As GridControl

    Property ListPresenter As BankSetupListPresenter
End Interface
