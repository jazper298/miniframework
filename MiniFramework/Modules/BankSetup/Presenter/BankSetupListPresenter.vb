﻿Public Class BankSetupListPresenter
    Private _view As iBankSetupList

    Private model As New BankSetupModel

    Public Sub New(ByVal view As iBankSetupList, Optional newList As Boolean = True)
        _view = view
        If newList Then
            refresh()
        End If
    End Sub

    Public Sub refresh()
        _view.dt = model.GetAllRecord()
    End Sub

    Property SelectedID
        Get
            Return _view.selectedID
        End Get
        Set(value)
            _view.selectedID = value
        End Set
    End Property


    Public Function delete()
        Dim dt As DataTable = model.GetRecord(SelectedID)
        If dt.Rows.Count > 0 Then
            If dt(0)("Status") = "Draft" Then
                Return model.Delete(SelectedID)
            End If
        End If
        Return False
    End Function

    Public Sub Deactivate()
        If model.Deactivate(SelectedID) Then
            refresh()
        End If
    End Sub

    Public Sub Reactivate()
        If model.Reactivate(SelectedID) Then
            refresh()
        End If
    End Sub
End Class
