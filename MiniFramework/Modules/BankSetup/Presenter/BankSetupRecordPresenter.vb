﻿Public Class BankSetupRecordPresenter

    Private _view As iBankSetupRecord
    Private data As New BankSetup
    Private model As New BankSetupModel

    Public Sub New(ByVal view As iBankSetupRecord, ByVal Optional refresh As Boolean = False)
        _view = view
        '_view.mode = _view.ListPresenter.returnMode    
    End Sub

    Public Sub edit(ByVal ID As Integer)
        Dim dt As DataTable

        dt = model.GetRecord(ID)

        Try

            data.BankSetupID = ID
            _view.title = dt(0)("Name").ToString

            _view.Name = dt(0)("Name").ToString
            _view.Description = dt(0)("Description").ToString
            _view.Status = dt(0)("Status")

            If _view.mode <> "view" Then
                _view.mode = "edit"
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Public Sub save()
        Cursor.Current = Cursors.WaitCursor

        data.statusID = 2
        data.Name = _view.Name
        data.Description = _view.Description

        If data.BankSetupID = 0 Then
            If model.Insert(data) Then
                MsgBox("Successfully Save", MsgBoxStyle.Information, "Bank Setup")
                edit(data.BankSetupID)
                _view.ListPresenter.refresh()
            End If
        Else
            If model.Update(data) Then
                MsgBox("Successfully Updated", MsgBoxStyle.Information, "Bank Setup")
                edit(data.BankSetupID)
                _view.ListPresenter.refresh()
            End If
        End If

    End Sub

    Public Sub saveDraft()
        Cursor.Current = Cursors.WaitCursor

        data.statusID = 1

        data.Name = _view.Name
        data.Description = _view.Description

        If data.BankSetupID = 0 Then
            If model.Insert(data) Then
                MsgBox("Successfully Save", MsgBoxStyle.Information, "Bank Setup")
                edit(data.BankSetupID)
                _view.ListPresenter.refresh()
            End If
        Else
            If model.Update(data) Then
                MsgBox("Successfully Updated", MsgBoxStyle.Information, "Bank Setup")
                edit(data.BankSetupID)
                _view.ListPresenter.refresh()
            End If
        End If


    End Sub

End Class
