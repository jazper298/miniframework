﻿Imports ERP

Public Class BankSetupModel


    Public Function Insert(ByRef obj As Object) As Object
        Dim sql As String = ""
        Dim dt As DataTable

        StartTransaction()

        Try
            sql = "
                INSERT into ""Treasury"".""BankSetup"" (""Name"",""Description"")
                VALUES ('" & obj.Name & "','" & obj.Description & "') RETURNING ""BankSetupID""
            "
            dt = DataSource(sql, True)

            sql = "
                INSERT INTO ""Treasury"".""BankSetupStatus"" (""StartTime"",""BankSetupID"",""BankSetupStatusTypeID"") VALUES (CURRENT_TIMESTAMP,'" & dt(0)("BankSetupID") & "','" & obj.statusID & "') 
            "
            DataSource(sql, True)

            commitQuery()

        Catch ex As Exception

            MsgBox(ex.Message)
            rollbackQuery()
            EndTransaction()

        End Try
        EndTransaction()

        obj.BankSetupID = dt(0)("BankSetupID")

        Return True
    End Function

    Public Function Delete(ID As Integer) As Object
        Dim sql As String

        sql = "DELETE FROM ""Treasury"".""BankSetup"" WHERE ""BankSetupID"" = '" & ID & "'"
        DataSource(sql)
        Return True
    End Function

    Public Function Update(obj As Object) As Object
        Dim sql As String = ""
        Dim dt As DataTable
        StartTransaction()

        Try

            sql = "
                UPDATE ""Treasury"".""BankSetup"" SET ""Name"" = '" & obj.Name & "',""Description"" = '" & obj.Description & "'
                WHERE ""BankSetupID"" = '" & obj.BankSetupID & "'                 
                "
            DataSource(sql, True)

            sql = "SELECT * FROM ""Treasury"".""BankSetupStatus"" WHERE  ""BankSetupID"" = '" & obj.BankSetupID & "' AND ""EndTime""  IS NULL  AND ""BankSetupStatusTypeID"" = '" & obj.statusID & "' "
            Dim ss = DataSource(sql, True)

            If ss.Rows.Count = 0 Then
                sql = "                
                    UPDATE ""Treasury"".""BankSetupStatus"" SET ""EndTime"" = CURRENT_TIMESTAMP WHERE  ""BankSetupID"" = '" & obj.BankSetupID & "'               
                "
                DataSource(sql, True)

                sql = "                
                    INSERT INTO ""Treasury"".""BankSetupStatus"" (""StartTime"",""BankSetupID"",""BankSetupStatusTypeID"") VALUES (CURRENT_TIMESTAMP,'" & obj.BankSetupID & "','" & obj.statusID & "')                 
                "
                DataSource(sql, True)
            End If

            commitQuery()

        Catch ex As Exception

            MsgBox(ex.Message)
            rollbackQuery()
            EndTransaction()

        End Try
        EndTransaction()

        Return True
    End Function

    Public Function GetRecord(ID As Integer) As Object
        Dim ObjectList As String = "
              
               SELECT
               ""Treasury"".""BankSetup"".""BankSetupID"",               
               ""Treasury"".""BankSetup"".""Name"",
               ""Treasury"".""BankSetup"".""Description"",
               ""Treasury"".""BankSetupStatusType"".""Name"" AS ""Status""
                FROM
                ""Treasury"".""BankSetup""
                INNER JOIN ""Treasury"".""BankSetupStatus"" ON ""BankSetupStatus"".""BankSetupID"" = ""BankSetup"".""BankSetupID""
                INNER JOIN ""Treasury"".""BankSetupStatusType"" ON ""BankSetupStatus"".""BankSetupStatusTypeID"" = ""BankSetupStatusType"".""BankSetupStatusTypeID""
                WHERE ""BankSetupStatus"".""EndTime"" is null AND ""BankSetup"".""BankSetupID"" = '" & ID & "'                
                "

        Return DataSource(ObjectList)
    End Function

    Public Function GetAllRecord() As Object
        Dim ObjectList As String = "
                    SELECT
                    ""Treasury"".""BankSetup"".""BankSetupID"" as ""_BankSetupID"",
                    ""Treasury"".""BankSetup"".""Name"",
                    ""Treasury"".""BankSetup"".""Description"",
                    ""Treasury"".""BankSetupStatusType"".""Name"" AS ""Status""
                    FROM
                    ""Treasury"".""BankSetup""
                    INNER JOIN ""Treasury"".""BankSetupStatus"" ON ""Treasury"".""BankSetupStatus"".""BankSetupID"" = ""Treasury"".""BankSetup"".""BankSetupID""
                    INNER JOIN ""Treasury"".""BankSetupStatusType"" ON ""Treasury"".""BankSetupStatus"".""BankSetupStatusTypeID"" = ""Treasury"".""BankSetupStatusType"".""BankSetupStatusTypeID""
                    WHERE ""Treasury"".""BankSetupStatus"".""EndTime"" is NULL
                "
        Return DataSource(ObjectList)
    End Function

    Public Function Deactivate(ID As Integer) As Object

        Dim sql As String = ""
        Dim dt As DataTable

        sql = "SELECT * FROM ""Treasury"".""BankSetupStatus"" WHERE ""BankSetupID"" = '" & ID & "' AND ""EndTime"" IS NULL AND ""BankSetupStatusTypeID"" = '1' "
        dt = DataSource(sql)


        If dt.Rows.Count > 0 Then
            MsgBox("Draft status cannot be deactivated", MsgBoxStyle.Information, "Deactivation")
            Return False
        End If

        StartTransaction()
        Try

            DataSource("UPDATE ""Treasury"".""BankSetupStatus"" SET ""EndTime""= current_timestamp WHERE ""BankSetupID""='" & ID & "' AND ""EndTime"" IS NULL RETURNING ""BankSetupStatusTypeID"" ", True)
            DataSource("INSERT INTO ""Treasury"".""BankSetupStatus"" (""StartTime"",""BankSetupID"",""BankSetupStatusTypeID"") VALUES (CURRENT_TIMESTAMP,'" & ID & "','3') ", True)

            commitQuery()
        Catch ex As Exception
            rollbackQuery()
            MsgBox(ex.Message)
            Return False
        End Try
        EndTransaction()
        Return True
    End Function

    Public Function Reactivate(ID As Integer) As Object
        Dim sql As String = ""
        Dim dt As DataTable

        sql = "SELECT * FROM ""Treasury"".""BankSetupStatus"" WHERE ""BankSetupID"" = '" & ID & "' AND ""EndTime"" IS NULL AND ""BankSetupStatusTypeID"" = '1' "
        dt = DataSource(sql)


        If dt.Rows.Count > 0 Then
            MsgBox("Draft status cannot be deactivated", MsgBoxStyle.Information, "Deactivation")
            Return False
        End If

        StartTransaction()
        Try

            DataSource("UPDATE ""Treasury"".""BankSetupStatus"" SET ""EndTime""= current_timestamp WHERE ""BankSetupID""='" & ID & "' AND ""EndTime"" IS NULL RETURNING ""BankSetupStatusTypeID"" ", True)
            DataSource("INSERT INTO ""Treasury"".""BankSetupStatus"" (""StartTime"",""BankSetupID"",""BankSetupStatusTypeID"") VALUES (CURRENT_TIMESTAMP,'" & ID & "','2') ", True)

            commitQuery()
        Catch ex As Exception
            rollbackQuery()
            MsgBox(ex.Message)
            Return False
        End Try
        EndTransaction()
        Return True
    End Function

End Class
